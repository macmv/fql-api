const http = require('http');
const fs = require('fs');

// http requires a seperate endpoint ip and port
const ENDPOINT = "127.0.0.1"
const ENDPOINT_PORT = "8000"
const TOKEN = "Bearer secret"

const options = {
  host: ENDPOINT,
  port: ENDPOINT_PORT,
  path: '/execute',
  method: 'POST',
  headers: {
    // Make sure we get json errors
    "Accept": "application/json",
    // This is how we authenticate
    "Authorization": TOKEN,
  },
}

// Make our actual request
let req = http.request(options, (res) => {
  res.on('data', buf => {
    let text = buf.toString();
    if (res.statusCode == 200) {
      console.log("Got result:", text);
    } else {
      console.log("Got error:");
      let json = JSON.parse(text);
      for (error in json["errors"]) {
        console.log(json["errors"][error]["printable_description"]);
      }
    }
  })
})

// Log any errors
req.on('error', error => {
  console.error(error)
})

// Finally, write our query in the POST body
req.write(fs.readFileSync('../../queries/simple.fql', 'utf8'))
req.end()
