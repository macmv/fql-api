# Javascript

This is the NodeJS example. We use the `http` library because it is builtin,
although something like `requests` would be simpler to use.

Run the `simple` example like so:

```bash
cd examples/javascript/simple
node index.js
```