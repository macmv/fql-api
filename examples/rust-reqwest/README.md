# Rust (using reqwest)

This is one of two Rust examples. Reqwest is an async HTTP client. It is
useful for production code, where you need a complete library that can
work with an existing async runtime.

However, for small projects, adding an async runtime may be unnecessary,
and will cause compile times to be much longer. So, you may also want to
look at the `rust-ureq` example.

Run the `simple` example like so:

```bash
cd examples/rust-reqwest/simple
cargo run
```