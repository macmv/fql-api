use reqwest::{
  header::{HeaderMap, HeaderValue},
  Client, StatusCode,
};
use serde::Deserialize;
use std::fs;

const ENDPOINT: &str = "http://localhost:8000";
const TOKEN: &str = "Bearer secret";

#[derive(Debug, Deserialize)]
struct JsonErrorResp {
  errors: Vec<JsonError>,
}

#[derive(Debug, Deserialize)]
struct JsonError {
  code:                  String,
  description:           String,
  printable_description: String,
  position:              Span,
}

#[derive(Debug, Deserialize)]
struct Span {
  start: Pos,
  end:   Pos,
}

#[derive(Debug, Deserialize)]
struct Pos {
  line: usize,
  col:  usize,
}

// Tokio will start an async executor for us
#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
  // Read our query to a string
  let query = fs::read_to_string("../../queries/simple.fql").unwrap_or_else(|e| {
    panic!("failed to read query: {}", e);
  });

  // I prefer to set these default headers, so that you can easily
  // make multiple requests without setting the headers again.
  let mut headers = HeaderMap::new();
  headers.insert("Accept", HeaderValue::from_static("application/json"));
  headers.insert("Authorization", HeaderValue::from_static(TOKEN));
  // We want to panic if this fails, as it should never fail
  let client = Client::builder().default_headers(headers).build().unwrap();

  // Make a POST request to ENDPOINT/execute
  let res = client.post(ENDPOINT.to_string() + "/execute").body(query).send().await?;

  // Make sure we check for errors
  if res.status() == StatusCode::OK {
    // Here, this can easily be parsed as JSON
    let value: i32 = res.json().await?;
    println!("Got response: {}", value);
  } else {
    // This will always be json, so we can display the printable error
    let errors: JsonErrorResp = res.json().await?;
    for error in errors.errors {
      println!("Got error:\n{}", error.printable_description);
    }
  }

  Ok(())
}
