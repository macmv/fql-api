# Go

Go has an HTTP client builtin. This makes this entire thing very easy
to write and run. Of all the languages, Go probably has the best
out-of-the box HTTP experience.

Run the `simple` example like so:

```bash
cd examples/go/simple
go run main.go
```