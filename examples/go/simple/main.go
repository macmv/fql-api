package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
)

const ENDPOINT = "http://localhost:8000"
const TOKEN = "Bearer secret"

type JsonErrorResp struct {
	Errors []JsonError `json:"errors"`
}

type JsonError struct {
	Code                 string `json:"code"`
	Description          string `json:"description"`
	PrintableDescription string `json:"printable_description"`
	Position             Span   `json:"position"`
}

type Span struct {
	Start Pos `json:"start"`
	End   Pos `json:"end"`
}

type Pos struct {
	Line uint `json:"line"`
	Col  uint `json:"col"`
}

func main() {
	// Read our query
	query, err := os.ReadFile("../../queries/simple.fql")
	if err != nil {
		log.Fatalln("Could not read query: ", err)
	}

	// Create a POST request to /execute
	req, err := http.NewRequest("POST", ENDPOINT+"/execute", bytes.NewBuffer(query))
	if err != nil {
		log.Fatalln("Could not make request: ", err)
	}

	// Make sure we get JSON errors
	req.Header.Set("Accept", "application/json")
	// This is how we authenticate with the database
	req.Header.Set("Authorization", TOKEN)

	// Make an HTTP client, and perform the request
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Fatalln("Could not send request: ", err)
	}
	defer res.Body.Close()

	// This is always going to be valid json
	data, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalln("Could not read response body: ", err)
	}

	if res.StatusCode == 200 {
		// This means we got valid data
		log.Println("Got response:", string(data))
	} else {
		// Print our error message
		var err JsonErrorResp
		_ = json.Unmarshal(data, &err)
		for _, err := range err.Errors {
			log.Println("Got error:\n" + err.PrintableDescription)
		}
	}
}
