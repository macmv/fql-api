require "net/http"
require "json"

# This is the proxy url
ENDPOINT = URI("http://localhost:8000")
# This is our authorization token
TOKEN = "Bearer secret"

Net::HTTP.start(ENDPOINT.host, ENDPOINT.port) do |http|
  # Make a POST request to /execute
  req = Net::HTTP::Post.new(ENDPOINT + "/execute")

  # Make sure we get json errors
  req["Accept"] = "application/json"
  # This is how we authenticate with the database
  req["Authorization"] = TOKEN

  # Read our query, and set it as the request body
  req.body = File.read("../../queries/simple.fql")

  # Make the actual request
  res = http.request(req)

  # Finally, we can print the result
  if res.is_a?(Net::HTTPSuccess)
    puts res.body
  else
    puts "Got error:"
    errors = JSON.parse(res.body)
    errors["errors"].each do |error|
      puts error["printable_description"]
    end
  end
end
