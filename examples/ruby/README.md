# Ruby

The ruby example is short and doesn't depend on any external libraries.

Run the `simple` example like so:

```bash
cd examples/ruby/simple
ruby main.rb
```