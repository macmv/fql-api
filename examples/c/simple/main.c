#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>

#define ENDPOINT "http://localhost:8000"
#define TOKEN "Bearer secret"

// Reads a file at the given path, and writes the values in `length` and `buf`.
// Returns 1 if anything failed.
int read_file(char* path, long* length, char** buf) {
  FILE *f = fopen(path, "rb");

  if (f) {
    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);
    *buf = malloc(*length);
    if (buf) {
      fread(*buf, 1, *length, f);
    }
    fclose(f);
    return 0;
  } else {
    return 1;
  }
}

int main() {
  CURL *curl;
  CURLcode res;

  // In windows, this will init the winsock stuff
  curl_global_init(CURL_GLOBAL_ALL);

  long query_length;
  char* query_buf;

  read_file("../../queries/simple.fql", &query_length, &query_buf);

  // Get a curl handle
  curl = curl_easy_init();
  if(curl) {
    struct curl_slist *chunk = NULL;
    // Make sure we get JSON errors
    chunk = curl_slist_append(chunk, "Accept: application/json");
    // This is how we authenticate
    char token[128];
    strcpy(token, "Authorization: ");
    strcat(token, TOKEN);
    chunk = curl_slist_append(chunk, token);

    // Set our headers
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

    // Make a POST request to ENDPOINT/execute
    char url[128];
    strcpy(url, ENDPOINT);
    strcat(url, "/execute");
    curl_easy_setopt(curl, CURLOPT_URL, url);

    // Set our POST body
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, query_length);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, query_buf);

    // If you need the POST body, you can set a callback to call
    // like so:
    //
    // curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    // curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

    // Perform the request, res will get the return code. This
    // will print our response if it worked.
    res = curl_easy_perform(curl);

    if (res == CURLE_OK) {
      printf("\n");
    } else {
      // Check for errors
      fprintf(stderr, "http request failed: %s\n", curl_easy_strerror(res));
    }

    // Make sure to cleanup
    curl_easy_cleanup(curl);
    curl_slist_free_all(chunk);
  }
  // Once all the curl requests are done, call this
  curl_global_cleanup();

  return 0;
}