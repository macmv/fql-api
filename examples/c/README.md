# C

This example is a little obscure. C is very verbose, so doing things like a full HTTP
request takes a lot of code. This example is more of a proof of concept, and is less
usable than say the Rust or Python examples.

Run the `simple` example like so:

```bash
cd examples/c/simple
make
```