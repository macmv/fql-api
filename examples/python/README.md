# Python

Note that this is python 3, not python 2.

Python doesn't have HTTP functionality builtin by default, so I decided
to use the `requests` library. This can be installed like so:

```bash
pip install requests
```

Run the `simple` example like so:

```bash
cd examples/python/simple
python main.py
```