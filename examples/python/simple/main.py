import requests

# This is the ip of the proxy
ENDPOINT = "http://localhost:8000"
TOKEN = "Bearer secret"

# Read our query into a string
with open("../../queries/simple.fql", "r") as f:
  query = f.read()

# Make our request here
r = requests.post(ENDPOINT + "/execute", data=query, headers={
  # Make sure that we get json error messages
  "Accept": "application/json",
  # This is how we authenticate with the server
  "Authorization": TOKEN,
})

if r.status_code == 200:
  # This means everything worked as expected, and we can print our result
  print(r.json())
else:
  # Errors should be handled seperately
  print("Got error:")
  for error in r.json()["errors"]:
    print(error["printable_description"])
