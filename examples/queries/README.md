# Queries

This is a collection of FQL queries that can be sent to the proxy. These
queries are loaded by each of the examples, and used as the POST body.