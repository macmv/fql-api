# Rust (using ureq)

This Rust example uses `ureq` as an HTTP client. This is a much
smaller library, and makes build times much shorter than `reqwest`.
However, it doesn't have any async functionality, so it might not
be what you are looking for in a production library.

See the `rust-reqwest` example as well.

Run the `simple` example like so:

```bash
cd examples/rust-ureq/simple
cargo run
```