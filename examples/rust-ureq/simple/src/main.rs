use serde::Deserialize;
use std::fs;

const ENDPOINT: &str = "http://localhost:8000";
const TOKEN: &str = "Bearer secret";

#[derive(Debug, Deserialize)]
struct JsonErrorResp {
  errors: Vec<JsonError>,
}

#[derive(Debug, Deserialize)]
struct JsonError {
  code:                  String,
  description:           String,
  printable_description: String,
  position:              Span,
}

#[derive(Debug, Deserialize)]
struct Span {
  start: Pos,
  end:   Pos,
}

#[derive(Debug, Deserialize)]
struct Pos {
  line: usize,
  col:  usize,
}

// This example doesn't use async, so we have a normal main function
fn main() -> Result<(), ureq::Error> {
  // Read our query to a string
  let query = fs::read_to_string("../../queries/simple.fql").unwrap_or_else(|e| {
    panic!("failed to read query: {}", e);
  });

  // Make a POST request to ENDPOINT/execute
  let res = ureq::post(&(ENDPOINT.to_string() + "/execute"))
    // Make sure we just get json errors
    .set("Accept", "application/json")
    // Set our authorization header
    .set("Authorization", TOKEN)
    // Send our FQL as the POST body
    .send_string(&query);

  // Handle errors
  match res {
    Ok(res) => {
      // Here, this can easily be parsed as JSON
      let value: i32 = res.into_json()?;
      println!("Got response: {}", value);
    }
    Err(ureq::Error::Status(code, res)) => {
      // This will always be json, so we can display the printable error
      let errors: JsonErrorResp = res.into_json()?;
      for error in errors.errors {
        println!("Got error:\n{}", error.printable_description);
      }
    }
    Err(e) => return Err(e),
  }

  Ok(())
}
