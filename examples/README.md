# Examples

Here is a collection of all of the examples I have written for this proxy.
It is simply an HTTP server, so these examples just document how to make
an HTTP request, set the Authorization header, and parse the response.

In all of these examples, there is a `TOKEN` constant defined at the top of
the main file. This is how we authenticate with the database. If you are
running the database locally, the value should be `Bearer secret`. If you are
running against a real database, the `TOKEN` value should be
`Bearer <secret key here>`. For example, if you got a secret key of `123456`
from FaunaDB, you would set `TOKEN` to `Bearer 123456`.

### Error format

Any errors returned by the API should look like this:

```json
{
  "errors": [{
    "code": "invalid argument",
    "description": "Number expected, String provided.",
    "printable_description": "<underlined source>",
    "span": {
      "start": {
        "line": 1,
        "col": 3
      },
      "end": {
        "line": 1,
        "col": 8
      }
    }
  }]
}
```

This format is parsed by all of the example code.
