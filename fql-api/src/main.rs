use rocket::launch;

#[launch]
fn rocket() -> _ { fql_api::build() }
