//! Converts a database response into a more user-friendly response.

use super::JsonHeaderResp;
use fql_parse::{
  parse::{ErrorVariant, LineColLocation, ParseError, Rule, Span},
  Expr, ExprKey, SpanOf,
};
use reqwest::header::HeaderMap;
use rocket::{
  http::Status,
  serde::json::{serde_json, serde_json::json},
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize)]
pub struct DatabaseResp {
  #[serde(default)]
  errors:   Vec<FQLError>,
  #[serde(default)]
  resource: serde_json::Value,
}

#[derive(Debug, Clone, Deserialize)]
pub struct FQLError {
  code:        String,
  description: String,
  #[serde(default)]
  position:    Vec<ExprKey>,
}

#[derive(Debug, Clone, Serialize)]
pub struct Error {
  code:        String,
  description: String,
  position:    ErrorSpan,
  // Human readable message, with the underlined source included.
  // printable_description: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct ErrorSpan {
  start: ErrorPos,
  end:   ErrorPos,
}
#[derive(Debug, Clone, Serialize)]
pub struct ErrorPos {
  line: usize,
  col:  usize,
}

impl Default for ErrorSpan {
  fn default() -> Self { ErrorSpan::new(ErrorPos::new(1, 1), ErrorPos::new(1, 2)) }
}
impl From<(usize, usize)> for ErrorPos {
  fn from(v: (usize, usize)) -> Self { ErrorPos::new(v.0, v.1) }
}
impl From<LineColLocation> for ErrorSpan {
  fn from(v: LineColLocation) -> Self {
    match v {
      LineColLocation::Pos(pos) => Self::from_tuples(pos, (pos.0, pos.1 + 1)),
      LineColLocation::Span(start, end) => Self::from_tuples(start, end),
    }
  }
}

impl ErrorSpan {
  pub fn new(start: ErrorPos, end: ErrorPos) -> Self { ErrorSpan { start, end } }
  pub fn from_tuples(start: (usize, usize), end: (usize, usize)) -> Self {
    ErrorSpan { start: start.into(), end: end.into() }
  }
  pub fn from_span(src: &str, pos: Span) -> Self {
    let mut total = 0;
    let mut start = None;
    let mut end = None;
    for (i, line) in src.lines().enumerate() {
      if total + line.len() > pos.start() && start.is_none() {
        start = Some(ErrorPos::new(i + 1, pos.start() - total + 1));
      }
      if total + line.len() > pos.end() && end.is_none() {
        end = Some(ErrorPos::new(i + 1, pos.end() - total + 1));
      }
      total += line.len();
    }
    ErrorSpan::new(start.unwrap_or(ErrorPos::new(1, 1)), end.unwrap_or(ErrorPos::new(1, 2)))
  }

  pub fn underline(&self, src: &str, desc: &str) -> String {
    // TODO
    String::new()
  }
}
impl ErrorPos {
  pub fn new(line: usize, col: usize) -> Self { ErrorPos { line, col } }
}

impl DatabaseResp {
  /// Converts `self` into a user friendly response.
  pub fn into_user(self, query: &Expr, headers: HeaderMap, src: &str) -> JsonHeaderResp {
    if self.errors.is_empty() {
      JsonHeaderResp { json: self.resource, status: Status::Ok, headers }
    } else {
      JsonHeaderResp {
        json: json!({
          "errors": self.generate_errors(query, src),
        }),
        status: Status::BadRequest,
        headers,
      }
    }
  }

  fn generate_errors(&self, query: &Expr, src: &str) -> Vec<Error> {
    self
      .errors
      .iter()
      .map(|fql| {
        let span = query.span_of(&fql.position).unwrap_or(Span::new(src, 0, 1).unwrap());
        // let message = span.underline(src, &fql.description);
        let err = ParseError::<Rule>::new_from_span(
          ErrorVariant::CustomError { message: fql.description.clone() },
          span,
        );
        Error {
          code:        fql.code.clone(),
          description: err.to_string(),
          position:    err.line_col.into(),
        }
      })
      .collect()
  }
}
