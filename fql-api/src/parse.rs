//! Converts a query into the wire protocol.

use super::{resp::ErrorSpan, JsonResp};
use fql_parse::{
  parse::{parse, LineColLocation},
  Expr,
};
use rocket::{http::Status, serde::json::serde_json::json};
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub enum Type {
  Number,
  Float,
  String,
}

pub fn to_ast<'a>(src: &'a str, args: HashMap<&str, &'a str>) -> Result<Expr<'a>, JsonResp> {
  parse(src, &args).map_err(|e| {
    let desc = e.to_string();
    let span = match e.line_col {
      LineColLocation::Pos((line, col)) => ErrorSpan::from_tuples((line, col), (line, col + 1)),
      LineColLocation::Span(start, end) => ErrorSpan::from_tuples(start, end),
    };
    // let message = span.underline(src, &desc);
    JsonResp {
      json:   json!({"errors": [{
        "code": "invalid FQL",
        "position": span,
        "description": desc,
        // "printable_description": message,
      }]}),
      status: Status::BadRequest,
    }
  })
}
