#[macro_use]
extern crate rocket;

mod parse;
mod resp;

use reqwest::{header::HeaderMap, ClientBuilder};
use resp::DatabaseResp;
use rocket::{
  fairing::AdHoc,
  http::{ContentType, Status},
  request::{FromRequest, Outcome},
  response,
  response::{content, Responder, Response},
  serde::{
    json::{serde_json, serde_json::json, Value as JsonValue},
    Serialize,
  },
  Build, Request, Rocket, State,
};
use std::collections::HashMap;

#[derive(serde::Deserialize)]
struct AppConfig {
  endpoint: String,
}

/// A wrapper type that lets me respond with a json value and a status code from
/// a query. This is used for any error reply, where we don't need to set custom
/// headers.
///
/// See also: [`JsonHeaderResp`]
#[derive(Debug)]
pub struct JsonResp {
  pub json:   JsonValue,
  pub status: Status,
}
/// A wrapper type that lets me respond with a json value, status code, and some
/// headers from a query. This is used for the database reply, where we need to
/// forward all the headers from the database.
///
/// See also: [`JsonResp`]
#[derive(Debug)]
pub struct JsonHeaderResp {
  pub json:    JsonValue,
  pub status:  Status,
  pub headers: HeaderMap,
}

impl<'r> Responder<'r, 'static> for JsonResp {
  fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
    Response::build_from(self.json.respond_to(req).unwrap())
      .status(self.status)
      .header(ContentType::JSON)
      .ok()
  }
}

impl<'r> Responder<'r, 'static> for JsonHeaderResp {
  fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
    let mut b = Response::build_from(self.json.respond_to(req).unwrap());
    b.status(self.status);
    for (k, v) in self.headers {
      let k = k.unwrap().as_str().to_owned();
      if k != "content-length" && k != "content-type" {
        b.raw_header(k, v.to_str().unwrap().to_owned());
      }
    }
    b.header(ContentType::JSON);
    b.ok()
  }
}

/// This parses the `Authorization` header from the request. This is required,
/// so the request will fail if the header is not present.
#[derive(Debug, Clone)]
struct Token<'r>(&'r str);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Token<'r> {
  type Error = &'static str;

  async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
    let token = request.headers().get_one("Authorization");
    match token {
      Some(tok) => Outcome::Success(Token(tok)),
      None => Outcome::Failure((Status::Unauthorized, "missing token")),
    }
  }
}

/// This parses the `X-FaunaDB-Endpoint` header. This is optional, so it returns
/// the default endpoint if none is given.
#[derive(Debug, Clone)]
struct Endpoint<'r>(&'r str);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Endpoint<'r> {
  type Error = &'static str;

  async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
    let token = request.headers().get_one("X-FaunaDB-Endpoint");
    match token {
      Some(url) => Outcome::Success(Endpoint(url)),
      None => Outcome::Success(Endpoint("db.fauna.com")),
    }
  }
}

/// This parses all URL query parameters into a HashMap. This is used for the
/// `URLQuery*` functions in the query.
#[derive(Debug, Clone)]
struct Arguments<'r>(HashMap<&'r str, &'r str>);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for Arguments<'r> {
  type Error = &'static str;

  async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
    match request.uri().query() {
      Some(query) => {
        let mut map = HashMap::new();
        for (name, value) in query.segments() {
          if let Some(_prev) = map.insert(name, value) {
            // Duplicate arguments means the client probably failed to sanitize their input.
            // Regardless of the reason, it doesn't make sense for us to accept this.
            return Outcome::Failure((Status::BadRequest, "duplicate query parameter"));
          }
        }
        Outcome::Success(Arguments(map))
      }
      None => Outcome::Success(Arguments(HashMap::new())),
    }
  }
}

/// This function handles the actual query from the client. The arguments to
/// this function are all converted from the request. For example, the [`Token`]
/// type is created from the `Authorization` header. See the docs for [`Token`],
/// [`Endpoint`], and [`Arguments`] for more.
///
/// The `src` is the post body, denoted by this macro call.
///
/// Finally, the `config` is the configuration for the whole server. This is how
/// we read information from the `Rocket.toml` file.
#[post("/execute", data = "<src>")]
async fn execute<'r>(
  token: Token<'r>,
  _endpoint_header: Endpoint<'r>,
  args: Arguments<'r>,
  src: String,
  config: &State<AppConfig>,
) -> Result<JsonHeaderResp, JsonResp> {
  let ast = parse::to_ast(&src, args.0)?;

  let (res, headers) =
    send_to_database(&config.endpoint, &token.0, &ast).await.map_err(|e| JsonResp {
      json:   json!({"errors": [{
        "code": "internal server error",
        "description": e.to_string(),
      }]}),
      status: Status::InternalServerError,
    })?;

  Ok(res.into_user(&ast, headers, &src))
}

/// Sends some data to the database. `endpoint` is the full URL of the database,
/// `token` is the `Authorization` header, and `data` is the wire protocol.
async fn send_to_database(
  endpoint: &str,
  token: &str,
  data: impl Serialize,
) -> Result<(DatabaseResp, HeaderMap), reqwest::Error> {
  let client = ClientBuilder::new().build()?;
  let res = client
    .post(endpoint)
    .header("Authorization", token)
    .body(dbg!(serde_json::to_string(&data).unwrap()))
    .send()
    .await?;
  let headers = res.headers().clone();
  Ok((res.json().await.unwrap(), headers))
}

#[get("/")]
async fn main<'r>() -> content::Html<String> {
  content::Html(comrak::markdown_to_html(
    include_str!("../doc/index.md"),
    &comrak::ComrakOptions::default(),
  ))
}

/// Builds a web server that will host the proxy. This is a library function so
/// that we can easily test it.
pub fn build() -> Rocket<Build> {
  rocket::build().attach(AdHoc::config::<AppConfig>()).mount("/", routes![execute, main])
}
