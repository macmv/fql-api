use super::parse;
use pretty_assertions::assert_eq;
use reqwest::{Client, StatusCode};
use serde::de::DeserializeOwned;
use serde_json::{json, Value};

/// Executes all the tests. Panics if any fail.
pub async fn test_all(endpoint: &str) {
  let tester = Tester::new(endpoint);
  for (src, result) in parse::parse_all("fql-test/fail_parse") {
    assert_eq!(
      tester.send_bad_request::<Value>(src.clone()).await,
      json!({"errors":
        [{
          "code": "invalid FQL",
          "description": result,
        }]
      }),
      "
fail_parse test failed.
request:
---
{}
---",
      src,
    );
  }
  for (src, result) in parse::parse_all("fql-test/fail_run") {
    assert_eq!(
      tester.send::<Value>(src.clone()).await,
      json!({"errors": [serde_json::from_str::<Value>(&result).unwrap()]}),
      "
fail_run test failed.
request:
---
{}
---",
      src,
    );
  }
  for (src, result) in parse::parse_all("fql-test/good") {
    assert_eq!(tester.send::<Value>(src).await, serde_json::from_str::<Value>(&result).unwrap());
  }
}

struct Tester<'a> {
  endpoint: &'a str,
}

impl<'a> Tester<'a> {
  pub fn new(endpoint: &'a str) -> Self { Tester { endpoint } }

  /// Sends a request, and makes sure the reponse is code 200 (Ok)
  pub async fn send<R: DeserializeOwned>(&self, body: String) -> R {
    self.send_code(body, StatusCode::OK).await
  }
  /// Sends a request, and makes sure the reponse is code 400 (Bad Request)
  pub async fn send_bad_request<R: DeserializeOwned>(&self, body: String) -> R {
    self.send_code(body, StatusCode::BAD_REQUEST).await
  }

  async fn send_code<R: DeserializeOwned>(&self, body: String, code: StatusCode) -> R {
    let res = Client::new()
      .post(self.endpoint)
      .header("Authorization", "Bearer secret")
      .body(body.clone())
      .send()
      .await
      .unwrap();
    if res.status() != code {
      let status = res.status();
      println!("while sending reqwest:\n---\n{}\n---", body);
      println!("got response body:\n---\n{}\n---", res.text().await.unwrap());
      panic!("send gave invalid code: {} (expected {})", status, code);
    }
    res.json().await.unwrap()
  }
}
