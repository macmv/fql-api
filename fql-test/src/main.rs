use docker_api::{
  container::{ContainerCreateOpts, PublishPort},
  Container, Docker,
};
use std::thread;

mod parse;
mod tests;

#[tokio::main]
async fn main() {
  let docker = Docker::new("unix:///var/run/docker.sock").unwrap();
  let fauna = start_fauna(&docker).await;
  start_proxy();

  println!("waiting for fauna to start (delaying for 10 seconds)");
  thread::sleep(std::time::Duration::from_secs(10));

  println!("running tests...");
  tests::test_all("http://localhost:8000/execute").await;
  println!("tests pass!");

  println!("stopping container...");
  fauna.stop(None).await.unwrap();
  println!("finished");
}

/// Starts the proxy
fn start_proxy() {
  println!("starting web server");
  tokio::spawn(fql_api::build().launch());
}

async fn start_fauna<'a>(docker: &'a Docker) -> Container<'a> {
  let path = std::env::current_dir().unwrap();
  let log_path = path.join("logs");
  let c = docker.containers().get("faunadb");
  println!("stopping existing container...");
  let _ = c.stop(None).await;
  // This sometimes fails if we don't want for docker to clean up the container
  thread::sleep(std::time::Duration::from_millis(500));
  println!("starting new container...");
  let c = docker
    .containers()
    .create(
      &ContainerCreateOpts::builder("fauna/faunadb")
        .name("faunadb")
        .expose(PublishPort::tcp(8443), 8443)
        .expose(PublishPort::tcp(8084), 8084)
        .volumes([format!("{}:/var/log/faunadb", log_path.display())])
        .auto_remove(true)
        .attach_stdout(true)
        .attach_stderr(true)
        .build(),
    )
    .await
    .unwrap();
  c.start().await.unwrap();
  println!("fauna started");
  c
}
