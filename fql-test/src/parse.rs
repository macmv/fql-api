use std::{fs, fs::ReadDir};

pub struct ParseIter {
  dir:     ReadDir,
  current: Vec<(String, String)>,
}

pub fn parse_all(dir: &str) -> ParseIter {
  ParseIter { dir: fs::read_dir(dir).unwrap(), current: vec![] }
}

impl Iterator for ParseIter {
  type Item = (String, String);

  fn next(&mut self) -> Option<Self::Item> {
    loop {
      if let Some(it) = self.current.pop() {
        return Some(it);
      }
      let ent = self.dir.next()?.unwrap();
      let path = ent.path();
      println!("running tests from {}", path.display());
      let file = fs::read_to_string(&path)
        .unwrap_or_else(|e| panic!("failed to read file from {}: {}", path.display(), e));
      for s in file.split("=====") {
        // s is now something like this:
        // ```
        // Add(1, 2)
        // ---
        // 3
        // ```
        let s = s.trim();
        if s.is_empty() {
          continue;
        }
        let mut inner = s.split("---");
        let first = inner
          .next()
          .unwrap_or_else(|| panic!("expected 2 inner sections in file {}", path.display()));
        let second = inner
          .next()
          .unwrap_or_else(|| panic!("expected 2 inner sections in file {}", path.display()));
        if inner.next().is_some() {
          panic!("expected 2 inner sections in file {}", path.display());
        }
        self.current.push((first.trim().into(), second.trim().into()));
      }
      // Because we pop from `current`, we are iterating over it in reverse order.
      // This makes our iterator return the test at the top of the file first (which
      // is what we want).
      self.current.reverse();
    }
  }
}
