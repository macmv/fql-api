use super::{FromQueryParam, Rule};
use pest::error::ErrorVariant;

impl FromQueryParam for bool {
  fn from_query_param(param: &str) -> Result<Self, ErrorVariant<Rule>> {
    param
      .parse::<bool>()
      .map_err(|e| ErrorVariant::CustomError { message: format!("in query param: {e}") })
  }
}
impl FromQueryParam for i64 {
  fn from_query_param(param: &str) -> Result<Self, ErrorVariant<Rule>> {
    param
      .parse::<i64>()
      .map_err(|e| ErrorVariant::CustomError { message: format!("in query param: {e}") })
  }
}
impl FromQueryParam for f64 {
  fn from_query_param(param: &str) -> Result<Self, ErrorVariant<Rule>> {
    param
      .parse::<f64>()
      .map_err(|e| ErrorVariant::CustomError { message: format!("in query param: {e}") })
  }
}
impl FromQueryParam for String {
  fn from_query_param(param: &str) -> Result<Self, ErrorVariant<Rule>> { Ok(param.into()) }
}
