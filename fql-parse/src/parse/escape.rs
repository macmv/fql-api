use super::Rule;
use pest::{
  error::{Error, ErrorVariant},
  iterators::Pair,
  Span,
};

fn parse_hex(chars: &mut impl Iterator<Item = (usize, char)>, num: usize) -> Option<u32> {
  let mut value = 0;
  for _ in 0..num {
    let (_, digit) = chars.next()?;
    let digit = digit.to_digit(16)?;
    value = value * 16 + digit;
  }
  Some(value)
}
fn scan_escape(
  chars: &mut impl Iterator<Item = (usize, char)>,
  buf: &mut String,
) -> Result<(), &'static str> {
  let (_, first) = chars.next().ok_or("expected an escape code")?;
  buf.push(match first {
    '"' => '"',
    'n' => '\n',
    'r' => '\r',
    't' => '\t',
    '\\' => '\\',
    'x' => {
      let value = parse_hex(chars, 2).ok_or("expected two valid hex characters after \\x")?;

      if value > 127 {
        return Err("hex escape must be valid ASCII");
      }
      (value as u8) as char
    }
    'u' => {
      let value = parse_hex(chars, 4).ok_or("expected four valid hex characters after \\u")?;

      std::char::from_u32(value).ok_or("invalid \\u escape code")?
    }
    _ => return Err("invalid escape code"),
  });
  Ok(())
}
fn escape_str(s: &str) -> Result<String, (&str, usize)> {
  let mut out = String::with_capacity(s.len());
  let mut chars = s.chars().enumerate();
  while let Some((pos, c)) = chars.next() {
    if c == '\\' {
      scan_escape(&mut chars, &mut out).map_err(|msg| (msg, pos))?;
    } else {
      out.push(c);
    }
  }
  Ok(out)
}
pub fn escape(span: Pair<Rule>) -> Result<String, Error<Rule>> {
  escape_str(span.as_str()).map_err(|(msg, pos)| {
    Error::new_from_span(
      ErrorVariant::CustomError { message: msg.into() },
      Span::new(span.as_str(), pos, pos + 2).unwrap(),
    )
  })
}
