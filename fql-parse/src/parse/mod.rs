use super::{Expr, ExprKind, QueryFunc};
use pest::{
  error::Error,
  iterators::{Pair, Pairs},
  Parser, Position,
};
use pest_derive::Parser;
use std::collections::HashMap;

mod escape;
mod impls;

pub use escape::escape;

pub use pest::{
  error::{Error as ParseError, ErrorVariant, LineColLocation},
  Span,
};

#[derive(Parser)]
#[grammar = "parse/fql.pest"]
struct FQLParser;

/// Creates an [`Expr`] from a query parameter. This is implemented for all
/// types that have a query function. For example, `URLQueryInt` will call this
/// with the type `i64`.
///
/// See the `impls.rs` file for implementations of this trait.
trait FromQueryParam {
  fn from_query_param(param: &str) -> Result<Self, ErrorVariant<Rule>>
  where
    Self: Sized;
}

fn query_func<'a, T: Into<ExprKind<'a>> + FromQueryParam>(
  span: Span<'a>,
  args: &mut Pairs<'a, Rule>,
  query_params: &HashMap<&str, &'a str>,
) -> Result<Expr<'a>, Error<Rule>> {
  let key = parse_expr(
    args.next().ok_or(Error::new_from_span(
      ErrorVariant::CustomError { message: "expected 1 or 2 arguments".into() },
      span.clone(),
    ))?,
    query_params,
  )?;
  let default = match args.next() {
    Some(p) => Some(parse_expr(p, query_params)?),
    None => None,
  };
  if args.next().is_some() {
    return Err(Error::new_from_span(
      ErrorVariant::CustomError { message: "expected 1 or 2 arguments".into() },
      span,
    ));
  }

  // Convert the key from an Expr into a String
  let key = match key.kind() {
    ExprKind::String(key) => key,
    _ => {
      return Err(Error::new_from_span(
        ErrorVariant::CustomError { message: "the first argument must be a string".into() },
        span,
      ))
    }
  };

  dbg!(&query_params, &key);
  if let Some(val) = query_params.get(key.as_str()) {
    Ok(Expr::new(
      span.clone(),
      T::from_query_param(val).map_err(|variant| Error::new_from_span(variant, span))?.into(),
    ))
  } else if let Some(def) = default {
    Ok(def)
  } else {
    Err(Error::new_from_span(
      ErrorVariant::CustomError { message: "no query param or default given".into() },
      span,
    ))
  }
}
fn query_present<'a>(
  span: Span<'a>,
  args: &mut Pairs<'a, Rule>,
  query_params: &HashMap<&str, &'a str>,
) -> Result<Expr<'a>, Error<Rule>> {
  let key = parse_expr(
    args.next().ok_or(Error::new_from_span(
      ErrorVariant::CustomError { message: "expected 1 argument".into() },
      span.clone(),
    ))?,
    query_params,
  )?;
  if args.next().is_some() {
    return Err(Error::new_from_span(
      ErrorVariant::CustomError { message: "expected 1 argument".into() },
      span,
    ));
  }

  // Convert the key from an Expr into a String
  let key = match key.kind() {
    ExprKind::String(key) => key,
    _ => {
      return Err(Error::new_from_span(
        ErrorVariant::CustomError { message: "the first argument must be a string".into() },
        span,
      ))
    }
  };

  Ok(Expr::new(span, ExprKind::Bool(query_params.contains_key(key.as_str()))))
}

fn handle_url_query_funcs<'a>(
  name: Span<'a>,
  span: Span<'a>,
  args: &mut Pairs<'a, Rule>,
  query_params: &HashMap<&str, &'a str>,
) -> Option<Result<Expr<'a>, Error<Rule>>> {
  Some(match name.as_str() {
    "URLQueryBool" => query_func::<bool>(span, args, query_params),
    "URLQueryInt" => query_func::<i64>(span, args, query_params),
    "URLQueryFloat" => query_func::<f64>(span, args, query_params),
    "URLQueryString" => query_func::<String>(span, args, query_params),
    "URLQueryPresent" => query_present(span, args, query_params),
    _ => return None,
  })
}

fn parse_map_pair<'a>(
  pair: Pair<'a, Rule>,
  query_params: &HashMap<&str, &'a str>,
) -> Result<(String, Expr<'a>), Error<Rule>> {
  assert_eq!(pair.as_rule(), Rule::pair);
  let mut iter = pair.into_inner();
  let key = iter.next().unwrap();
  let val = iter.next().unwrap();
  Ok((
    match key.as_rule() {
      Rule::string => escape(key.into_inner().next().unwrap())?,
      Rule::ident => key.as_str().into(),
      _ => unreachable!("unexpected rule for key: {:?}", key),
    },
    parse_expr(val, query_params)?,
  ))
}

fn parse_expr<'a>(
  pair: Pair<'a, Rule>,
  query_params: &HashMap<&str, &'a str>,
) -> Result<Expr<'a>, Error<Rule>> {
  Ok(Expr::new(
    pair.as_span(),
    match pair.as_rule() {
      Rule::null => ExprKind::Null,
      Rule::boolean => ExprKind::Bool(pair.as_str() == "true"),
      Rule::number => ExprKind::Int(pair.as_str().parse().unwrap()),
      Rule::string => ExprKind::String(escape(pair.into_inner().next().unwrap())?),
      Rule::array => ExprKind::Array(
        pair.into_inner().map(|pair| parse_expr(pair, query_params)).collect::<Result<_, _>>()?,
      ),
      Rule::object => ExprKind::Object(
        pair
          .into_inner()
          .map(|pair| parse_map_pair(pair, query_params))
          .collect::<Result<_, _>>()?,
      ),
      Rule::call => {
        let mut pairs = pair.into_inner();

        let name = pairs.next().unwrap();

        let args = pairs.next().unwrap();
        let args_span = args.as_span();
        let mut args = args.into_inner();

        if let Some(res) =
          handle_url_query_funcs(name.as_span(), args_span, &mut args, query_params)
        {
          return res;
        } else {
          return Ok(Expr::new(
            name.as_span(),
            ExprKind::Func(Box::new(QueryFunc::parse(
              name.as_span(),
              args.map(|p| parse_expr(p, query_params)),
            )?)),
          ));
        }
      }
      r => unreachable!("unexpected rule {:?}", r),
    },
  ))
}

pub fn parse<'a>(
  src: &'a str,
  query_params: &HashMap<&str, &'a str>,
) -> Result<Expr<'a>, Error<Rule>> {
  let mut pairs = FQLParser::parse(Rule::fql, src)?;
  parse_expr(pairs.next().unwrap(), query_params)
}

#[test]
fn simple() {
  let query_params = HashMap::new();
  match parse("Sqrt(5)", &query_params) {
    Ok(v) => {
      dbg!(v);
    }
    Err(e) => {
      println!("{}", e);
      panic!();
    }
  }
}

#[test]
fn arrays() {
  let query_params = HashMap::new();
  assert!(
    matches!(parse("[5, 6]", &query_params).unwrap().kind(), ExprKind::Array(v) if v.len() == 2)
  );
}
#[test]
fn objects() {
  let query_params = HashMap::new();
  assert!(matches!(parse("{a: 5, b: 6}", &query_params).unwrap_or_else(|e| {
      println!("{}", e); panic!();
    }).kind(), ExprKind::Object(v) if v.len() == 2));
}
