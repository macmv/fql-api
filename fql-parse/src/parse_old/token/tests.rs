use super::*;

#[test]
fn read_ident() -> Result<()> {
  let mut tok = Tokenizer::new("hello world".as_bytes());
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("hello", Span::new(Pos::new(1, 1, 0), Pos::new(1, 6, 5)))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("world", Span::new(Pos::new(1, 7, 6), Pos::new(1, 12, 11)))))
  );
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_keyword() -> Result<()> {
  let mut tok = Tokenizer::new("{ (   )   }world fn main()".as_bytes());
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::OpenBrace,
      pos:   Span::new(Pos::new(1, 1, 0), Pos::new(1, 2, 1)),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::OpenParen,
      pos:   Span::new(Pos::new(1, 3, 2), Pos::new(1, 4, 3)),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::CloseParen,
      pos:   Span::new(Pos::new(1, 7, 6), Pos::new(1, 8, 7)),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::CloseBrace,
      pos:   Span::new(Pos::new(1, 11, 10), Pos::new(1, 12, 11)),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("world", Span::new(Pos::new(1, 12, 11), Pos::new(1, 17, 16)))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("fn", Span::new(Pos::new(1, 18, 17), Pos::new(1, 20, 19)))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("main", Span::new(Pos::new(1, 21, 20), Pos::new(1, 25, 24)))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::OpenParen,
      pos:   Span::new(Pos::new(1, 25, 24), Pos::new(1, 26, 25)),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Punct(PunctPos {
      inner: Punct::CloseParen,
      pos:   Span::new(Pos::new(1, 26, 25), Pos::new(1, 27, 26)),
    }))
  );
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_int() -> Result<()> {
  let mut tok = Tokenizer::new("0 5 -10".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 0));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 10));
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_float() -> Result<()> {
  let mut tok = Tokenizer::new("0.0 5.0 -10.0".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_float() == 0.0));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_float() == 5.0));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_float() == 10.0));
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_str() -> Result<()> {
  let mut tok = Tokenizer::new(r#""Hello" "world" "big""gaming""#.as_bytes());
  assert!(
    matches!(tok.read().unwrap(), Token::Literal(l) if l.inner == Literal::String("Hello".into()))
  );
  assert!(
    matches!(tok.read().unwrap(), Token::Literal(l) if l.inner == Literal::String("world".into()))
  );
  assert!(
    matches!(tok.read().unwrap(), Token::Literal(l) if l.inner == Literal::String("big".into()))
  );
  assert!(
    matches!(tok.read().unwrap(), Token::Literal(l) if l.inner == Literal::String("gaming".into()))
  );
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_minus() -> Result<()> {
  let mut tok = Tokenizer::new("-".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("-5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("- 5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("-5.0".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_float() == 5.0));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("- 5.0".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_float() == 5.0));

  let mut tok = Tokenizer::new("3 - -5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("3 - - 5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("3 -- 5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Punct(k) if k.value() == &Punct::Neg));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}

#[test]
fn read_eof() -> Result<()> {
  let mut tok = Tokenizer::new("".as_bytes());
  assert_eq!(tok.peek(0), Ok(&Token::EOF));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("(3)".as_bytes());
  assert_eq!(tok.peek(3), Ok(&Token::EOF));
  assert!(matches!(tok.peek(2).unwrap(), t if t.is_punct(Punct::CloseParen)));
  assert!(matches!(tok.peek(1).unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.peek(0).unwrap(), t if t.is_punct(Punct::OpenParen)));

  let mut tok = Tokenizer::new("(3)".as_bytes());
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek_opt(1), Ok(None));
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::CloseParen)));
  assert_eq!(tok.peek_opt(0), Ok(None));

  let mut tok = Tokenizer::new("(3)".as_bytes());
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek(1), Ok(&Token::EOF));
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::CloseParen)));
  assert_eq!(tok.peek(0), Ok(&Token::EOF));

  let mut tok = Tokenizer::new("(3)".as_bytes());
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek(1), Ok(&Token::EOF));
  assert!(matches!(tok.read().unwrap(), t if t.is_punct(Punct::CloseParen)));
  // There was a bug where `read` would insert an EOF into the peeked stack,
  // and peek_opt would not check anything in the stack, and it would return
  // Ok(EOF) here, which is invalid.
  assert_eq!(tok.peek_opt(0), Ok(None));
  Ok(())
}

#[test]
fn read_comment() -> Result<()> {
  let mut tok = Tokenizer::new("// hello".as_bytes());
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("5 // hello".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("//hello\n5".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("//hello\nhumans".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "humans"));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("words\n//hello".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("words\n  //hello".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF));

  let mut tok = Tokenizer::new("  //hello\n  words".as_bytes());
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF));
  Ok(())
}
