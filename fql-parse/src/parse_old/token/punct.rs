use std::fmt;

macro_rules! puncts {
  [ $($name:ident => $val:expr,)* ] => {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Punct {
      $(
        $name,
      )*
    }

    impl Punct {
      pub fn from_token(s: &str) -> Option<Self> {
        match s {
          $(
            $val => Some(Punct::$name),
          )*
          _ => None,
        }
      }
    }

    impl fmt::Display for Punct {
      fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
          $(
            Punct::$name => f.write_str($val),
          )*
        }
      }
    }
  }
}

#[rustfmt::skip]
puncts![
  OpenBrace  => "{",
  CloseBrace => "}",
  OpenParen  => "(",
  CloseParen => ")",
  OpenArr    => "[",
  CloseArr   => "]",

  Comma  => ",",
  Colon  => ":",
  Neg    => "-", // Needed for negatives
  Dollar => "$", // Needed for variables

  // Add => "+",
  // Sub => "-",
  // Mul => "*",
  // Div => "/",
  // Mod => "%",
  // Exp => "**",
  //
  // AddAssign  => "+=",
  // SubAssign  => "-=",
  // MulAssign  => "*=",
  // DivAssign  => "/=",
  // ModAssign  => "%=",
  // ExpAssign  => "**=",
  //
  // Inc => "++",
  // Dec => "--",
  //
  // Not => "!",
  // Eq => "==",
  // Neq => "!=",
  // Less => "<",
  // Greater => ">",
  // GTE => ">=",
  // LTE => "<=",
  //
  // BinXor => "^",
  // BinOr => "|",
  // BinAnd => "&",
  //
  // LogicOr => "||",
  // LogicAnd => "&&",
];
