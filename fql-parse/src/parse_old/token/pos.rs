/// A token span. This contains a start and end position. It represents a region
/// of text. Once errors are fully implemented, it will be used to generate
/// messages like this:
///
/// ```text
/// 22 if a == b {
/// 23   println(words)
///              ^^^^^ Unknown variable `words`
/// 24 }
/// ```
///
/// Or
///
/// ```text
/// 22 if a == b {
/// 23   println(,)
///              ^ Unexpected token `,`, expected an expression
/// 24 }
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Span {
  start: Pos,
  end:   Pos,
}
/// A position within a source file. When an error is generated, the index will
/// be used to search both forwards and backwards for a newline. If found, the
/// line will be printed in the error message. If not found, a subsection of the
/// line will be printed. See [`new`](Self::new) for more.
///
/// The index will be used to choose which character the position is
/// referencing. The column will be printed in the error message, but that is
/// not validated with the newlines that may have been found.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Pos {
  pub(super) line:  usize,
  pub(super) col:   usize,
  pub(super) index: usize,
}

impl Default for Span {
  /// Returns the default span, which ranges from line 1, column 1 to
  /// line 1, column 2. This will use the file at index 0. If this is
  /// ever used to underline something, it will not make any sense.
  ///
  /// This should only ever be used when a placeholder is needed, for
  /// example building a node in the tree before you've parsed everything
  /// needed.
  fn default() -> Self { Span::new(Pos::new(1, 1, 0), Pos::new(1, 2, 1)) }
}
impl Default for Pos {
  /// Returns the default position, which is line 1, column 1.
  fn default() -> Self { Pos::new(1, 1, 0) }
}

impl Span {
  /// Creates a new span with the given start and end positions. The end is
  /// not inclusive. So for example the span from column 2 to column 4 would
  /// result in this range:
  /// ```text
  /// 1234
  ///  ^^ <- The span from column 2 to 4
  /// ```
  pub fn new(start: Pos, end: Pos) -> Self { Span { start, end } }
  /// Creates a new span at the end of the file. This should only be used in
  /// rare situations. Specifically, this is the position that Token::EOF
  /// returns when generating error messages.
  ///
  /// Generally, things like unmatched parenthesis should not produce errors
  /// at the eof, as that is just annoying. This is only used so that cli
  /// errors are easy to generate.
  pub fn eof() -> Self { Span { start: Pos::new(!0, !0, !0), end: Pos::new(!0, !0, !0) } }
  /// Returns the start of the range. This is inclusive, so this character
  /// should be counted as the first character in the range.
  pub fn start(&self) -> Pos { self.start }
  /// Returns the end of the range. This is exclusive, so everything before
  /// this character should be counted.
  pub fn end(&self) -> Pos { self.end }
  /// Produces an underline for the given source code, with the given message
  /// displayed next to that message. This will only attempt to look for a
  /// newline 100 characters ahead and backwards from `self.index` within `src`,
  /// in order to avoid issues where the whole file is on one line.
  pub fn underline(&self, src: &str, msg: &str) -> String {
    if self == &Span::eof() {
      let (num, last) = src.lines().enumerate().last().unwrap();
      // Line and column numbers start at 1
      let actual = Span::new(
        Pos::new(num + 1, last.len(), src.len() - 1),
        Pos::new(num + 1, last.len() + 1, src.len()),
      );
      return actual.underline(src, msg);
    }
    self.underline_inner(src, msg)
  }
  fn underline_inner(&self, src: &str, msg: &str) -> String {
    let mut out = String::new();

    let lines_away = 2;
    let num_width = (self.start.line + lines_away).to_string().len();

    let (prev, next) = Span::find_newlines(src, self.start.index, 100);
    for (i, pos) in prev.iter().enumerate().rev().take(lines_away + 1).rev() {
      if i == prev.len() - 1 {
        break;
      }
      let pos = *pos;
      let line = self.start.line - (prev.len() - i - 1);
      out += &format!(
        " {}{} {}\n",
        " ".repeat(num_width - line.to_string().len()),
        line.to_string(),
        &src[pos..prev[i + 1] - 1]
      );
    }
    if let Some(start) = prev.last() {
      if let Some(end) = next.first() {
        out += &format!(
          " {}{} {}\n",
          " ".repeat(num_width - self.start.line.to_string().len()),
          self.start.line.to_string(),
          &src[*start..*end]
        );
      }
    } else {
    }
    if self.end.col <= self.start.col {
      out += &format!(" {}{} {}\n", " ".repeat(num_width), "^".repeat(self.start.col), msg);
    } else {
      out += &format!(
        " {}{} {}\n",
        " ".repeat(self.start.col + num_width),
        "^".repeat(self.end.col - self.start.col),
        msg,
      )
      .to_string();
    }
    for (i, pos) in next.iter().enumerate().take(lines_away) {
      if i == next.len() - 1 {
        break;
      }
      let line = self.start.line + i + 1;
      out += &format!(
        " {}{} {}\n",
        " ".repeat(num_width - line.to_string().len()),
        line.to_string(),
        &src[*pos + 1..next[i + 1]]
      );
    }
    out
  }

  /// Finds the newlines in the source string. This will look at most `dist`
  /// characters away from index.
  ///
  /// The first returned array is a list of newlines found before the index. All
  /// of these are offset by one, so that a value of zero means we found the
  /// start of the source string.
  ///
  /// The second returned array is a list of newlines after index. These are not
  /// offset, so that `src.len()` can be used to signify the end of the source
  /// string.
  fn find_newlines(src: &str, index: usize, dist: usize) -> (Vec<usize>, Vec<usize>) {
    let mut iter;
    let mut prev_newlines = vec![];
    let mut next_newlines = vec![];
    if index <= dist {
      iter = src.chars().enumerate().skip(0);
      prev_newlines.push(0);
    } else {
      iter = src.chars().enumerate().skip(index - dist);
    }
    for _ in 0..dist * 2 {
      let (pos, c) = match iter.next() {
        Some(v) => v,
        None => {
          next_newlines.push(src.len());
          break;
        }
      };
      if c == '\n' {
        if pos > index {
          next_newlines.push(pos)
        } else {
          prev_newlines.push(pos + 1)
        }
      }
    }
    (prev_newlines, next_newlines)
  }
}
impl Pos {
  /// Creates a new position. The line and column are for human readability,
  /// and the index is how this fetches the source code when generating errors.
  /// So an invalid combination of these values would result in some source
  /// code being displayed for the wrong line.
  ///
  /// Generally, you shouldn't need to call this. The tokenizer will generate
  /// spans for every token it parses, so you should just re-use the positions
  /// it returns instead of building your own.
  pub fn new(line: usize, col: usize, index: usize) -> Self { Pos { line, col, index } }
  /// Returns the line number of this position. Used only for human readability.
  pub fn line(&self) -> usize { self.line }
  /// Returns the column number of this position. Used only for human
  /// readability.
  pub fn col(&self) -> usize { self.col }
  /// Returns the index of this position. This is the byte index into the
  /// source, not the char index.
  pub fn index(&self) -> usize { self.index }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn underline() {
    let src = "words\nhere\nlots\nof\nwords";
    let res = Span::new(Pos::new(3, 2, 12), Pos::new(3, 4, 14)).underline(src, "big gaming");
    println!("{}", res);
    assert_eq!(res, format!(" 1 words\n 2 here\n 3 lots\n    ^^ big gaming\n 4 of\n 5 words\n",));
  }
}
