use super::token::{Ident, ParseError, Pos, Punct, Result, Span, Token, Tokenizer};
use crate::{Expr, ExprKind, QueryFunc, VarArgs};
use std::{collections::HashMap, io};

pub struct Parser<'a, R> {
  tokens:     Tokenizer<R>,
  query_args: HashMap<&'a str, &'a str>,
  types:      HashMap<String, Expr>,
}

impl<'a, R> Parser<'a, R> {
  pub fn new(tokens: Tokenizer<R>, query_args: HashMap<&'a str, &'a str>) -> Self {
    Parser { tokens, query_args, types: HashMap::new() }
  }

  pub fn tokens(&mut self) -> &mut Tokenizer<R> { &mut self.tokens }
}

pub trait Parse {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self>
  where
    Self: Sized;
}

trait FromQueryString {
  fn from_query_string(s: &str) -> Result<Self>
  where
    Self: Sized;
}

pub trait IntoExpr {
  fn into_expr(self, pos: Span) -> Expr;
}

impl<R: io::Read> Parser<'_, R> {
  /// Parses the given type.
  pub fn parse<T: Parse>(&mut self) -> Result<T> { T::parse(self) }
  /// Parses the given type, then returns an error if there is any remaining
  /// input.
  pub fn parse_all<T: Parse>(&mut self) -> Result<T> {
    let val = T::parse(self)?;
    if let Ok(Some(tok)) = self.tokens().peek_opt(0) {
      Err(ParseError::unexpected(tok.clone(), "end of input"))
    } else {
      Ok(val)
    }
  }

  fn query_param<T: IntoExpr + Parse + FromQueryString>(&mut self, start: Pos) -> Result<Expr> {
    self.tokens().expect(Punct::OpenParen)?;
    let name: String = self.parse()?;
    let default: Option<T> = if self.tokens().peek(0)?.is_punct(Punct::Comma) {
      self.tokens().read().unwrap();
      if !self.tokens().peek(0)?.is_punct(Punct::CloseParen) {
        Some(self.parse()?)
      } else {
        None
      }
    } else {
      None
    };
    self.tokens().expect(Punct::CloseParen)?;
    let end = self.tokens().pos();
    // TODO: Make sure we aren't parsing the same query arg as another type.
    let value = if let Some(default) = default {
      self.query_args.get(&name.as_str()).map(|s| T::from_query_string(s)).unwrap_or(Ok(default))?
    } else {
      self.query_args.get(&name.as_str()).map(|s| T::from_query_string(s)).ok_or_else(|| {
        ParseError::undefined("variable", Ident::new(name.clone(), Span::new(start, end)))
      })??
    };
    Ok(value.into_expr(Span::new(start, end)))
  }

  fn query_param_present(&mut self, start: Pos) -> Result<Expr> {
    self.tokens().expect(Punct::OpenParen)?;
    let name: String = self.parse()?;
    self.tokens().expect(Punct::CloseParen)?;
    let end = self.tokens().pos();
    Ok(Expr::new(
      Span::new(start, end),
      ExprKind::Bool(self.query_args.contains_key(&name.as_str())),
    ))
  }
}

enum Number {
  Int(i64),
  Double(f64),
}

impl IntoExpr for bool {
  fn into_expr(self, pos: Span) -> Expr { Expr::new(pos, ExprKind::Bool(self)) }
}
impl IntoExpr for Number {
  fn into_expr(self, pos: Span) -> Expr {
    Expr::new(
      pos,
      match self {
        Number::Int(v) => ExprKind::Int(v),
        Number::Double(v) => ExprKind::Float(v),
      },
    )
  }
}
impl IntoExpr for i64 {
  fn into_expr(self, pos: Span) -> Expr { Expr::new(pos, ExprKind::Int(self)) }
}
impl IntoExpr for f64 {
  fn into_expr(self, pos: Span) -> Expr { Expr::new(pos, ExprKind::Float(self)) }
}
impl IntoExpr for String {
  fn into_expr(self, pos: Span) -> Expr { Expr::new(pos, ExprKind::String(self)) }
}

impl Parse for () {
  fn parse<R: io::Read>(_p: &mut Parser<R>) -> Result<Self> { Ok(()) }
}
impl Parse for bool {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let tok = p.tokens().read()?.ident("a boolean")?;
    if tok.as_ref() == "true" {
      Ok(true)
    } else if tok.as_ref() == "false" {
      Ok(false)
    } else {
      Err(ParseError::unexpected(Token::Ident(tok), "a boolean"))
    }
  }
}
impl FromQueryString for bool {
  fn from_query_string(s: &str) -> Result<Self> {
    if s == "true" {
      Ok(true)
    } else if s == "false" {
      Ok(false)
    } else {
      Err(ParseError::Custom(Span::eof(), format!("Invalid boolean `{}`", s)))
    }
  }
}
impl Parse for Number {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let lit = p.tokens().read()?.lit("a number")?;
    if lit.is_int() {
      Ok(Number::Int(lit.unwrap_int()))
    } else if lit.is_float() {
      Ok(Number::Double(lit.unwrap_float()))
    } else {
      Err(ParseError::unexpected(Token::Literal(lit), "a number"))
    }
  }
}
// TODO: This should also parse floats!
impl FromQueryString for Number {
  fn from_query_string(s: &str) -> Result<Self> {
    Ok(Number::Int(s.parse::<i64>().map_err(|e| ParseError::Custom(Span::eof(), e.to_string()))?))
  }
}
impl Parse for i64 {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let lit = p.tokens().read()?.lit("an integer")?;
    if lit.is_int() {
      Ok(lit.unwrap_int())
    } else {
      Err(ParseError::unexpected(Token::Literal(lit), "an integer"))
    }
  }
}
impl FromQueryString for i64 {
  fn from_query_string(s: &str) -> Result<Self> {
    s.parse::<i64>().map_err(|e| ParseError::Custom(Span::eof(), e.to_string()))
  }
}
impl Parse for f64 {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let lit = p.tokens().read()?.lit("a double")?;
    if lit.is_float() {
      Ok(lit.unwrap_float())
    } else {
      Err(ParseError::unexpected(Token::Literal(lit), "a double"))
    }
  }
}
impl FromQueryString for f64 {
  fn from_query_string(s: &str) -> Result<Self> {
    s.parse::<f64>().map_err(|e| ParseError::Custom(Span::eof(), e.to_string()))
  }
}
impl Parse for String {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let lit = p.tokens().read()?.lit("a string")?;
    if lit.is_str() {
      Ok(lit.unwrap_str())
    } else {
      Err(ParseError::unexpected(Token::Literal(lit), "a string"))
    }
  }
}
impl FromQueryString for String {
  fn from_query_string(s: &str) -> Result<Self> { Ok(s.into()) }
}

impl Parse for Expr {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    match p.tokens().peek(0)? {
      // A function call
      t if t.is_ident() => {
        let start = t.pos().start();
        let ident = p.tokens().read().unwrap().ident("").unwrap();
        if ident.as_ref() == "true" {
          return Ok(Expr::new(ident.pos(), ExprKind::Bool(true)));
        } else if ident.as_ref() == "false" {
          return Ok(Expr::new(ident.pos(), ExprKind::Bool(false)));
        }
        let expr = match ident.as_ref() {
          "URLQueryPresent" => p.query_param_present(start)?,
          "URLQueryBool" => p.query_param::<bool>(start)?,
          "URLQueryNumber" => p.query_param::<Number>(start)?,
          "URLQueryInt" => p.query_param::<i64>(start)?,
          "URLQueryDouble" => p.query_param::<f64>(start)?,
          "URLQueryString" => p.query_param::<String>(start)?,
          _ => {
            p.tokens().expect(Punct::OpenParen)?;
            let func = QueryFunc::parse(ident, p)?;
            p.tokens().expect(Punct::CloseParen)?;
            Expr::new(Span::new(start, p.tokens().pos()), ExprKind::Func(Box::new(func)))
          }
        };
        Ok(expr)
      }
      // A number or string
      t if t.is_lit() => {
        let lit = p.tokens().read().unwrap().lit("").unwrap();
        if lit.is_int() {
          Ok(Expr::new(lit.pos(), ExprKind::Int(lit.unwrap_int())))
        } else if lit.is_float() {
          Ok(Expr::new(lit.pos(), ExprKind::Float(lit.unwrap_float())))
        } else if lit.is_str() {
          Ok(Expr::new(lit.pos(), ExprKind::String(lit.unwrap_str())))
        } else {
          unreachable!()
        }
      }
      // An object
      t if t.is_punct(Punct::OpenBrace) => {
        let start = t.pos().start();
        let end;
        p.tokens().read().unwrap();
        let mut out = HashMap::new();
        if p.tokens().peek(0)?.is_punct(Punct::CloseBrace) {
          let t = p.tokens().read().unwrap();
          end = t.pos().end();
          return Ok(Expr::new(Span::new(start, end), ExprKind::Object(out)));
        }
        loop {
          let key = p.tokens().read()?.ident("an object key")?.into();
          p.tokens.expect(Punct::Colon)?;
          let val = p.parse()?;
          out.insert(key, val);
          let found_comma = p.tokens().peek(0)?.is_punct(Punct::Comma);
          if found_comma {
            p.tokens().read().unwrap();
          }
          if p.tokens().peek(0)?.is_punct(Punct::CloseBrace) {
            let t = p.tokens().read().unwrap();
            end = t.pos().end();
            break;
          }
          if !found_comma {
            p.tokens().expect(Punct::Comma)?;
          }
        }
        Ok(Expr::new(Span::new(start, end), ExprKind::Object(out)))
      }
      // An array
      t if t.is_punct(Punct::OpenArr) => {
        let start = t.pos().start();
        let end;
        p.tokens().read().unwrap();
        let mut out = vec![];
        if p.tokens().peek(0)?.is_punct(Punct::CloseArr) {
          let t = p.tokens().read().unwrap();
          end = t.pos().end();
          return Ok(Expr::new(Span::new(start, end), ExprKind::Array(out)));
        }
        loop {
          let val = p.parse()?;
          out.push(val);
          let found_comma = p.tokens().peek(0)?.is_punct(Punct::Comma);
          if found_comma {
            p.tokens().read().unwrap();
          }
          if p.tokens().peek(0)?.is_punct(Punct::CloseArr) {
            let t = p.tokens().read().unwrap();
            end = t.pos().end();
            break;
          }
          if !found_comma {
            p.tokens().expect(Punct::Comma)?;
          }
        }
        Ok(Expr::new(Span::new(start, end), ExprKind::Array(out)))
      }
      t => Err(ParseError::unexpected(t.clone(), "an expression")),
    }
  }
}

impl Parse for VarArgs<Expr> {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let mut out = vec![];
    while !p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
      out.push(p.parse()?);

      let found_comma = p.tokens().peek(0)?.is_punct(Punct::Comma);
      if found_comma {
        p.tokens().read().unwrap();
      }
      if p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
        break;
      }
      if !found_comma {
        p.tokens().expect(Punct::Comma)?;
      }
    }
    Ok(VarArgs::new(out))
  }
}

impl<T> Parse for Option<T>
where
  T: Parse,
{
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    if p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
      Ok(None)
    } else {
      p.parse().map(|v| Some(v))
    }
  }
}

impl<T> Parse for Box<T>
where
  T: Parse,
{
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> { p.parse().map(|v| Box::new(v)) }
}
