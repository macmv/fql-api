mod parser;
pub mod token;

pub use parser::Parser;

#[cfg(test)]
pub mod test;

use super::Expr;
use std::collections::HashMap;
use token::{Result, Tokenizer};

pub fn parse_expr(text: &str) -> Result<Expr> {
  let mut parser = Parser::new(Tokenizer::new(text.as_bytes()), HashMap::new());
  parser.parse_all()
}
