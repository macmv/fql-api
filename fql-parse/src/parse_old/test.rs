use super::parse_expr;
use crate::{
  parse::{
    token::{ParseError, Pos, Punct, PunctPos, Span, Token},
    Parser, Tokenizer,
  },
  Expr, QueryFunc, VarArgs,
};
use serde_json::json;
use std::collections::HashMap;

#[track_caller]
fn parse(src: &str) -> Expr {
  match Parser::new(Tokenizer::new(src.as_bytes()), HashMap::new()).parse() {
    Ok(v) => v,
    Err(e) => panic!("failed to parse src: {}\nerror: {:?}", src, e),
  }
}
fn parse_invalid(src: &str) -> ParseError {
  Parser::new(Tokenizer::new(src.as_bytes()), HashMap::new()).parse::<Expr>().unwrap_err()
}

macro_rules! map {
  { $($key:ident: $val:expr),* } => {{
    let mut m = HashMap::new();
    $(
      m.insert(stringify!($key).into(), $val);
    )*
    m
  }}
}
// This macro just creates spans on line 1.
macro_rules! span {
  ( $from:literal..$to:literal ) => {
    Span::new(Pos::new(1, $from, $from - 1), Pos::new(1, $to, $to - 1))
  };
}

#[test]
fn basic_functions() {
  parse_invalid("");
  assert_eq!(parse(r#""hello""#), Expr::new(span!(1..8), "hello"));
  assert_eq!(parse("1234"), Expr::new(span!(1..5), 1234));
  assert_eq!(parse("1.2"), Expr::new(span!(1..4), 1.2));
  assert_eq!(parse("true"), Expr::new(span!(1..5), true));
  assert_eq!(parse_invalid("True"), ParseError::Unexpected(Token::EOF, "(".into()));

  assert_eq!(
    parse("[2, 3]"),
    Expr::new(span!(1..7), vec![Expr::new(span!(2..3), 2), Expr::new(span!(5..6), 3)])
  );
  assert_eq!(
    parse("[2, 3,]"),
    Expr::new(span!(1..8), vec![Expr::new(span!(2..3), 2), Expr::new(span!(5..6), 3)])
  );
  assert_eq!(
    parse_invalid("[2, 3,,]"),
    ParseError::Unexpected(
      Token::Punct(PunctPos::new(Punct::Comma, span!(7..8))),
      "an expression".into(),
    ),
  );
  assert_eq!(parse("[]"), Expr::new(span!(1..3), vec![]));
  assert_eq!(
    parse_invalid("[,]"),
    ParseError::Unexpected(
      Token::Punct(PunctPos::new(Punct::Comma, span!(2..3))),
      "an expression".into(),
    ),
  );

  assert_eq!(parse("{foo: 5}"), Expr::new(span!(1..9), map! { foo: Expr::new(span!(7..8), 5) }));
  assert_eq!(parse("{foo: 5,}"), Expr::new(span!(1..10), map! { foo: Expr::new(span!(7..8), 5) }));
  assert_eq!(
    parse_invalid("{foo: 5,,}"),
    ParseError::Unexpected(
      Token::Punct(PunctPos::new(Punct::Comma, span!(9..10))),
      "an object key".into(),
    ),
  );
  assert_eq!(
    parse("{foo: 5, bar: 3}"),
    Expr::new(
      span!(1..17),
      map! { foo: Expr::new(span!(7..8), 5), bar: Expr::new(span!(15..16), 3) }
    )
  );
  assert_eq!(
    parse("{foo: 5, bar: 3,}"),
    Expr::new(
      span!(1..18),
      map! { foo: Expr::new(span!(7..8), 5), bar: Expr::new(span!(15..16), 3) }
    )
  );
  assert_eq!(parse("{}"), Expr::new(span!(1..3), map! {}));
  assert_eq!(
    parse_invalid("{,}"),
    ParseError::Unexpected(
      Token::Punct(PunctPos::new(Punct::Comma, span!(2..3))),
      "an object key".into(),
    ),
  );

  // This is only possible to catch on the database. VarArgs are always zero or
  // more arguments on the parser, but for this specific function, it requires 1
  // or more arguments. This is an inconsistency that I have opted to not handle
  // (even though the dashboard shell does).
  assert_eq!(parse("Add()"), Expr::func(span!(1..6), QueryFunc::Add(VarArgs::new(vec![]))));
  assert_eq!(
    parse("Add(5, 6)"),
    Expr::func(
      span!(1..10),
      QueryFunc::Add(VarArgs::new(vec![Expr::new(span!(5..6), 5), Expr::new(span!(8..9), 6)]))
    )
  );
}

#[test]
fn simple_expr() {
  // parse_expr(r#"Create(2, 3)"#).unwrap();
  assert_eq!(
    parse_expr(r#"Ref(3)"#).unwrap(),
    Expr::func(span!(1..7), QueryFunc::Ref(Expr::new(span!(5..6), 3), None))
  );
  assert_eq!(
    parse_expr(r#"Create(Ref(Collection("hello")), 5)"#).unwrap(),
    Expr::func(
      span!(1..36),
      QueryFunc::Create(
        Expr::func(
          span!(8..32),
          QueryFunc::Ref(
            Expr::func(
              span!(12..31),
              QueryFunc::Collection(Expr::new(span!(23..30), "hello"), None)
            ),
            None
          )
        ),
        Expr::new(span!(34..35), 5),
      )
    )
  );
  let expr = parse_expr(
    r#"Create(Ref(Collection("hello"), "2"), {
      data: {
        name: "Orwen",
      }
    })"#,
  )
  .map_err(|e| println!("err: {}", e))
  .unwrap();
  assert_eq!(
    serde_json::to_value(&expr).unwrap(),
    json!(
      {
        "create": {
          "ref": {
            "collection": "hello"
          },
          "id": "2"
        },
        "params": {
          "object": {
            "data": {
              "object": {
                "name": "Orwen"
              }
            }
          }
        }
      }
    )
  );
  parse_expr(r#"Add(1, 2))"#).unwrap_err();
}
