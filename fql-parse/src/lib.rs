use pest::Span;
use serde::{
  ser::{SerializeMap, SerializeSeq},
  Serialize, Serializer,
};
use std::collections::HashMap;

mod funcs;
pub mod parse;
mod span;

pub use funcs::{QueryFunc, VarArgs};
pub use span::ExprKey;

#[derive(Serialize)]
pub struct CollectionRef {
  collection: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr<'a> {
  inner: Box<ExprInner<'a>>,
}

#[derive(Debug, Clone, PartialEq)]
struct ExprInner<'a> {
  pos:  Span<'a>,
  kind: ExprKind<'a>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ExprKind<'a> {
  Null,
  Bool(bool),
  Int(i64),
  Float(f64),
  String(String),
  Array(Vec<Expr<'a>>),
  Object(HashMap<String, Expr<'a>>),
  Func(Box<QueryFunc<'a>>),
}

pub trait SpanOf {
  /// This returns the span of the given path. This is how we convert an error
  /// position from the database into a line/column number.
  fn span_of<'a>(&'a self, path: &[ExprKey]) -> Option<Span<'a>>;
}

impl<'a> From<QueryFunc<'a>> for ExprKind<'a> {
  fn from(func: QueryFunc<'a>) -> Self { Self::Func(Box::new(func)) }
}
impl From<bool> for ExprKind<'_> {
  fn from(v: bool) -> Self { Self::Bool(v) }
}
impl From<i64> for ExprKind<'_> {
  fn from(v: i64) -> Self { Self::Int(v) }
}
impl From<f64> for ExprKind<'_> {
  fn from(v: f64) -> Self { Self::Float(v) }
}
impl From<String> for ExprKind<'_> {
  fn from(s: String) -> Self { Self::String(s) }
}
impl<'a> From<Vec<Expr<'a>>> for ExprKind<'a> {
  fn from(v: Vec<Expr<'a>>) -> Self { Self::Array(v) }
}
impl<'a> From<HashMap<String, Expr<'a>>> for ExprKind<'a> {
  fn from(v: HashMap<String, Expr<'a>>) -> Self { Self::Object(v) }
}

/*
impl From<serde_json::Value> for Expr {
  fn from(val: serde_json::Value) -> Expr { (&val).into() }
}

impl From<&serde_json::Value> for Expr {
  fn from(val: &serde_json::Value) -> Expr {
    if let Some(num) = val.as_i64() {
      Expr::Int(num)
    } else if let Some(num) = val.as_u64() {
      Expr::Int(num.try_into().expect("u64 does not fit into i64"))
    } else if let Some(num) = val.as_f64() {
      Expr::Float(num)
    } else if let Some(text) = val.as_str() {
      Expr::String(text.into())
    } else if let Some(val) = val.as_bool() {
      Expr::Bool(val)
    } else if val.is_null() {
      Expr::Null
    } else if let Some(val) = val.as_array() {
      Expr::Array(val.into_iter().map(|v| v.into()).collect())
    } else if let Some(val) = val.as_object() {
      Expr::Object(val.into_iter().map(|(k, v)| (k.clone(), v.into())).collect())
    } else {
      unreachable!()
    }
  }
}
*/

impl Serialize for Expr<'_> {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    match self.kind() {
      ExprKind::Null => serializer.serialize_unit(),
      ExprKind::Int(v) => serializer.serialize_i64(*v),
      ExprKind::Float(v) => serializer.serialize_f64(*v),
      ExprKind::String(v) => serializer.serialize_str(v),
      ExprKind::Bool(v) => serializer.serialize_bool(*v),
      ExprKind::Array(arr) => {
        let mut seq = serializer.serialize_seq(Some(arr.len()))?;
        for elem in arr {
          seq.serialize_element(elem)?;
        }
        seq.end()
      }
      ExprKind::Object(data) => {
        let mut map = serializer.serialize_map(Some(1))?;
        map.serialize_entry("object", data)?;
        map.end()
      }
      ExprKind::Func(func) => func.serialize(serializer),
    }
  }
}

impl<'a> Expr<'a> {
  pub fn new(pos: Span<'a>, kind: impl Into<ExprKind<'a>>) -> Self {
    Expr { inner: Box::new(ExprInner { pos, kind: kind.into() }) }
  }
  pub fn func(pos: Span<'a>, func: impl Into<QueryFunc<'a>>) -> Self {
    Expr::new(pos, ExprKind::Func(Box::new(func.into())))
  }
  pub fn pos(&self) -> Span<'a> { self.inner.pos.clone() }
  pub fn kind(&self) -> &ExprKind { &self.inner.kind }
}
