use super::{parse::Rule, Expr, ExprKey, ExprKind, Span, SpanOf};
use pest::error::{Error, ErrorVariant};
use serde::{ser::SerializeMap, Serialize, Serializer};
use serde_json::{json, to_value};
use std::collections::HashMap;

fql_build_macros::query_func! {
  // Basic functions

  Ref("ref": Expr<'a>, "id": Option<Expr<'a>>) {
    if let Some(id) = id {
      out.insert("ref", to_value(r).unwrap());
      out.insert("id", to_value(id).unwrap());
    } else {
      out.insert("@ref", to_value(r).unwrap());
    }
  },
  Abort("abort": Expr<'a>),
  At("at": Expr<'a>, "expr": Expr<'a>),
  Let("let": Expr<'a>, "in": Expr<'a>) {
    match l.kind() {
      ExprKind::Object(bindings) => {
        out.insert(
          "let",
          json!(
            bindings
            .iter()
            .map(|(k, v)| {
              let mut map = HashMap::new();
              map.insert(k, to_value(v).unwrap());
              map
            })
            .collect::<Vec<_>>()
          ));
        out.insert("in", to_value(i).unwrap());
      },
      _ => {
        // NOTE: This is what the javascript driver does, although this
        // seems invalid. This essentially allows this syntax:
        //
        // ```
        // Let(5, Do(...))
        // ```
        //
        // The above makes no sense, and should produce an error (in my
        // opinion). However, I do not have error production setup, so
        // this currently just produces an empty variables table (just
        // like the javascript driver).
        out.insert("let", json!([]));
        out.insert("in", to_value(i).unwrap());
        // panic!("cannot pass non-object to Let");
      }
    }
  },
  Var("var": Expr<'a>),
  If("if": Expr<'a>, "then": Expr<'a>, "else": Expr<'a>),
  Do("do": VarArgs<Expr<'a>>) {
    // Oddity with the wire protocol; this is always serialized as an array.
    // Nothing else appears to do this, as everything else uses the `varargs`
    // function to serialize arguments (which makes a 1 length array into the
    // one value of that array, and anything else into the array itself).
    out.insert("do", json!(d.inner()));
  },
  Lambda("lambda": Expr<'a>, "expr": Expr<'a>),
  Call("call": Expr<'a>, "arguments": VarArgs<Expr<'a>>),
  Query("query": Expr<'a>),

  // Collection functions

  // Map and ForEach don't follow the norm; they use `collection` as the name
  // for the first argument, even though most other functions don't. This (once
  // again) matches the javascript driver :P
  Map("collection": Expr<'a>, "map": Expr<'a>),
  ForEach("collection": Expr<'a>, "foreach": Expr<'a>),
  Filter("collection": Expr<'a>, "filter": Expr<'a>),
  // These functions are back to normal
  Take("take": Expr<'a>, "collection": Expr<'a>),
  Drop("drop": Expr<'a>, "collection": Expr<'a>),
  Prepend("prepend": Expr<'a>, "collection": Expr<'a>),
  Append("append": Expr<'a>, "collection": Expr<'a>),
  IsEmpty("is_empty": Expr<'a>),
  IsNonEmpty("is_nonempty": Expr<'a>),
  IsNumber("is_number": Expr<'a>),
  IsDouble("is_double": Expr<'a>),
  IsInteger("is_integer": Expr<'a>),
  IsBoolean("is_boolean": Expr<'a>),
  IsNull("is_null": Expr<'a>),
  IsBytes("is_bytes": Expr<'a>),
  IsTimestamp("is_timestamp": Expr<'a>),
  IsDate("is_date": Expr<'a>),
  IsString("is_string": Expr<'a>),
  IsArray("is_array": Expr<'a>),
  IsObject("is_object": Expr<'a>),
  IsRef("is_ref": Expr<'a>),
  IsSet("is_set": Expr<'a>),
  IsDoc("is_doc": Expr<'a>),
  IsLambda("is_lambda": Expr<'a>),
  IsCollection("is_collection": Expr<'a>),
  IsDatabase("is_database": Expr<'a>),
  IsIndex("is_index": Expr<'a>),
  IsFunction("is_function": Expr<'a>),
  IsKey("is_key": Expr<'a>),
  IsToken("is_token": Expr<'a>),
  IsCredentials("is_credentials": Expr<'a>),
  IsRole("is_role": Expr<'a>),

  // Read functions

  Get("get": Expr<'a>, "ts": Option<Expr<'a>>),
  KeyFromSecret("key_from_secret": Expr<'a>),
  Reduce("reduce": Expr<'a>, "initial": Expr<'a>, "collection": Expr<'a>),
  Paginate("paginate": Expr<'a>, "size": Option<Expr<'a>>, "after": Option<Expr<'a>>, "before": Option<Expr<'a>>, "sources": Option<Expr<'a>>),
  Exists("exists": Expr<'a>, "ts": Option<Expr<'a>>),

  // Write functions

  Create("create": Expr<'a>, "params": Expr<'a>),
  Update("update": Expr<'a>, "params": Expr<'a>),
  Replace("replace": Expr<'a>, "params": Expr<'a>),
  Delete("delete": Expr<'a>),
  Insert("insert": Expr<'a>, "ts": Expr<'a>, "action": Expr<'a>, "params": Expr<'a>),
  Remove("remove": Expr<'a>, "ts": Expr<'a>, "action": Expr<'a>),
  CreateCollection("create_collection": Expr<'a>),
  CreateDatabase("create_database": Expr<'a>),
  CreateIndex("create_index": Expr<'a>),
  CreateKey("create_key": Expr<'a>),
  CreateFunction("create_function": Expr<'a>),
  CreateRole("create_role": Expr<'a>),
  CreateAccessProvider("create_aaccess_provider": Expr<'a>),

  // Sets

  Singleton("singleton": Expr<'a>),
  Events("events": Expr<'a>),
  Match("match": Expr<'a>, "terms": VarArgs<Expr<'a>>),
  Union("union": VarArgs<Expr<'a>>),
  Merge("merge": Expr<'a>, "with": Expr<'a>, "lambda": Option<Expr<'a>>),
  Intersection("intersection": VarArgs<Expr<'a>>),
  Difference("difference": VarArgs<Expr<'a>>),
  Distinct("distinct": Expr<'a>),
  Join("join": Expr<'a>, "with": Expr<'a>),
  Range("range": Expr<'a>, "from": Expr<'a>, "to": Expr<'a>),

  // Authentication

  Login("login": Expr<'a>, "params": Expr<'a>),
  Logout("logout": Expr<'a>),
  Identify("identify": Expr<'a>, "password": Expr<'a>),
  CurrentIdentity("current_identity": ()),
  HasCurrentIdentity("has_current_identity": ()),
  CurrentToken("current_token": ()),
  HasCurrentToken("has_current_token": ()),

  // String functions

  Concat("concat": Expr<'a>, "separator": Option<Expr<'a>>),
  Casefold("casefold": Expr<'a>, "normalizer": Option<Expr<'a>>),
  ContainsStr("containsstr": Expr<'a>, "search": Expr<'a>),
  ContainsStrRegex("containsstrregex": Expr<'a>, "pattern": Expr<'a>),
  StartsWith("startswith": Expr<'a>, "search": Expr<'a>),
  EndsWith("endswith": Expr<'a>, "search": Expr<'a>),
  RegexEscape("regexescape": Expr<'a>),
  FindStr("findstr": Expr<'a>, "find": Expr<'a>, "start": Option<Expr<'a>>),
  FindStrRegex("findstrregex": Expr<'a>, "pattern": Expr<'a>, "start": Option<Expr<'a>>, "num_results": Option<Expr<'a>>),
  Length("length": Expr<'a>),
  Lowercase("lowercase": Expr<'a>),
  LTrim("ltrim": Expr<'a>),
  NGram("ngram": Expr<'a>, "min": Option<Expr<'a>>, "max": Option<Expr<'a>>),
  Repeat("repeat": Expr<'a>, "number": Option<Expr<'a>>),
  ReplaceStr("replacestr": Expr<'a>, "find": Expr<'a>, "replace": Expr<'a>),
  ReplaceStrRegex("replacestrregex": Expr<'a>, "pattern": Expr<'a>, "replace": Expr<'a>, "first": Option<Expr<'a>>),
  RTrim("rtrim": Expr<'a>),
  Space("space": Expr<'a>),
  SubString("substring": Expr<'a>, "start": Expr<'a>, "length": Option<Expr<'a>>),
  TitleCase("titlecase": Expr<'a>),
  Trim("trim": Expr<'a>),
  Uppercase("uppercase": Expr<'a>),
  Format("format": Expr<'a>, "values": VarArgs<Expr<'a>>),

  // Time functions

  Time("time": Expr<'a>),
  Epoch("epoch": Expr<'a>, "unit": Expr<'a>),
  TimeAdd("time_add": Expr<'a>, "offset": Expr<'a>, "unit": Expr<'a>),
  TimeSubtract("time_subtract": Expr<'a>, "offset": Expr<'a>, "unit": Expr<'a>),
  TimeDiff("time_diff": Expr<'a>, "other": Expr<'a>, "unit": Expr<'a>),
  Date("date": Expr<'a>),
  Now("now": Expr<'a>),

  // Misc functions

  NextId("next_id": ()),
  NewId("new_id": ()),

  Database("database": Expr<'a>, "scope": Option<Expr<'a>>),
  Index("index": Expr<'a>, "scope": Option<Expr<'a>>),
  Collection("collection": Expr<'a>, "scope": Option<Expr<'a>>),
  Function("function": Expr<'a>, "scope": Option<Expr<'a>>),
  Role("role": Expr<'a>, "scope": Option<Expr<'a>>),

  AccessProviders("access_providers": Expr<'a>),
  Collections("collections": Expr<'a>),
  Databases("databases": Expr<'a>),
  Indexes("indexes": Expr<'a>),
  Functions("functions": Expr<'a>),
  Roles("roles": Expr<'a>),
  Keys("keys": Expr<'a>),
  Tokens("tokens": Expr<'a>),
  Credentials("credentials": Expr<'a>),

  // Conditional functions (considered misc)

  Equals("equals": VarArgs<Expr<'a>>),
  Contains("contains": Expr<'a>, "in": Expr<'a>),
  ContainsValue("contains_value": Expr<'a>, "in": Expr<'a>),
  ContainsField("contains_field": Expr<'a>, "in": Expr<'a>),
  ContainsPath("contains_path": Expr<'a>, "in": Expr<'a>),
  Select("select": Expr<'a>, "from": Expr<'a>, "default": Option<Expr<'a>>),
  SelectAll("select_all": Expr<'a>, "from": Expr<'a>),

  // Math

  Abs("abs": Expr<'a>),
  Add("add": VarArgs<Expr<'a>>),
  BitAnd("bitand": VarArgs<Expr<'a>>),
  BitNot("bitnot": VarArgs<Expr<'a>>),
  BitOr("bitor": VarArgs<Expr<'a>>),
  BitXor("bitxor": VarArgs<Expr<'a>>),
  Ceil("ceil": Expr<'a>),
  Divide("divide": VarArgs<Expr<'a>>),
  Floor("floor": Expr<'a>),
  Max("max": VarArgs<Expr<'a>>),
  Min("min": VarArgs<Expr<'a>>),
  Modulo("modulo": VarArgs<Expr<'a>>),
  Multiply("multiply": VarArgs<Expr<'a>>),
  Round("round": Expr<'a>, "precision": Option<Expr<'a>>),
  Subtract("subtract": VarArgs<Expr<'a>>),
  Sign("sign": Expr<'a>),
  Sqrt("sqrt": Expr<'a>),
  Trunc("trunc": Expr<'a>, "precision": Option<Expr<'a>>),

  Count("count": Expr<'a>),
  Sum("sum": Expr<'a>),
  Mean("mean": Expr<'a>),
  Any("any": Expr<'a>),
  All("all": Expr<'a>),
  Acos("acos": Expr<'a>),
  Asin("asin": Expr<'a>),
  Atan("atan": Expr<'a>),
  Cos("cos": Expr<'a>),
  Cosh("num": Expr<'a>),
  Degrees("degrees": Expr<'a>),
  Exp("exp": Expr<'a>),
  Hypot("hypot": Expr<'a>, "b": Expr<'a>),
  Ln("ln": Expr<'a>),
  Log("log": Expr<'a>),
  Pow("pow": Expr<'a>, "exp": Option<Expr<'a>>),
  Radians("radians": Expr<'a>),
  Sin("sin": Expr<'a>),
  Sinh("sinh": Expr<'a>),
  Tan("tan": Expr<'a>),
  Tanh("tanh": Expr<'a>),

  // Logical functions

  LT("lt": VarArgs<Expr<'a>>),
  LTE("lte": VarArgs<Expr<'a>>),
  GT("gt": VarArgs<Expr<'a>>),
  GTE("gte": VarArgs<Expr<'a>>),
  And("and": VarArgs<Expr<'a>>),
  Or("or": VarArgs<Expr<'a>>),
  Not("not": Expr<'a>),
  ToString("to_string": Expr<'a>),
  ToNumber("to_number": Expr<'a>),
  ToObject("to_object": Expr<'a>),
  ToArray("to_array": Expr<'a>),
  ToDouble("to_double": Expr<'a>),
  ToInteger("to_integer": Expr<'a>),
  ToTime("to_time": Expr<'a>),
  ToSeconds("to_seconds": Expr<'a>),
  ToMillis("to_millis": Expr<'a>),
  ToMicros("to_micros": Expr<'a>),

  // More time functions

  DayOfWeek("day_of_week": Expr<'a>),
  DayOfYear("day_of_year": Expr<'a>),
  DayOfMonth("day_of_month": Expr<'a>),
  Hour("hour": Expr<'a>),
  Minute("minute": Expr<'a>),
  Second("second": Expr<'a>),
  Month("month": Expr<'a>),
  Year("year": Expr<'a>),
  ToDate("to_date": Expr<'a>),

  // Functions in the wrong order? This is 1:1 with the JS driver, so I'm leaving them like this.

  MoveDatabase("move_database": Expr<'a>, "to": Expr<'a>),
  Documents("documents": Expr<'a>),
  Reverse("reverse": Expr<'a>),
  AccessProvider("access_provider": Expr<'a>, "scope": Option<Expr<'a>>),
}

#[derive(Debug, Clone, PartialEq)]
pub struct VarArgs<T> {
  inner: Vec<T>,
}

impl<T> Serialize for VarArgs<T>
where
  T: Serialize,
{
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    // This matches the python impl of variable length args
    if self.inner.len() == 1 {
      self.inner[0].serialize(serializer)
    } else {
      self.inner.serialize(serializer)
    }
  }
}

impl<T> VarArgs<T> {
  pub fn new(inner: Vec<T>) -> Self { VarArgs { inner } }

  pub fn inner(&self) -> &[T] { &self.inner }

  pub fn span_of(&self, path: &[ExprKey]) -> Option<Span>
  where
    T: SpanOf,
  {
    if path.is_empty() {
      return None;
    }
    let idx = path[0].as_int()?;
    self.inner.get(usize::try_from(idx).ok()?)?.span_of(&path[1..])
  }
}

/*
#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn it_works() {
    crate::parse::parse_expr("Add(Add(Add(Add(Add(1, 21), 21), 1), 2), 6)").unwrap();
    panic!();
    /*
    let func = QueryFunc::Create(
      QueryFunc::Ref(QueryFunc::Collection("hello".into(), None).into(), None).into(),
      serde_json::json!({"data": {"name": "Orwen"}}).into(),
    );
    println!("{}", serde_json::to_string(&func).unwrap());
    */
  }
}
*/
