//! This module finds the span of a path into an expression tree. This is how we
//! find line/column numbers from the database.

use super::{Expr, ExprKind, SpanOf};
use pest::Span;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
pub enum ExprKey {
  Int(i64),
  String(String),
}

impl ExprKey {
  pub fn as_int(&self) -> Option<i64> {
    match self {
      Self::Int(v) => Some(*v),
      _ => None,
    }
  }
  pub fn as_str(&self) -> Option<&str> {
    match self {
      Self::String(s) => Some(s.as_ref()),
      _ => None,
    }
  }
}

impl SpanOf for Expr<'_> {
  fn span_of<'a>(&'a self, path: &[ExprKey]) -> Option<Span<'a>> {
    if path.is_empty() {
      return Some(self.pos());
    }
    println!("getting value with key: {:?}, self: {:?}", path, self);
    match self.kind() {
      ExprKind::Func(func) => func.span_of(path),
      ExprKind::Array(arr) => {
        let idx = path[0].as_int()?;
        arr.get(usize::try_from(idx).ok()?)?.span_of(&path[1..])
      }
      ExprKind::Object(map) => {
        let object = path[0].as_str()?;
        if object != "object" {
          return None;
        }
        if path.len() == 1 {
          Some(self.pos())
        } else {
          let key = path[1].as_str()?;
          map.get(key)?.span_of(&path[2..])
        }
      }
      _ => None,
    }
  }
}

/*
#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    parse::token::{Pos, Span},
    QueryFunc, VarArgs,
  };

  #[test]
  fn test_span_of_add() {
    let span_1 = Span::new(Pos::new(1, 2, 0), Pos::new(1, 3, 0));
    let span_2 = Span::new(Pos::new(1, 3, 0), Pos::new(1, 4, 0));
    let expr = Expr::new(
      Span::default(),
      ExprKind::Func(Box::new(QueryFunc::Collection(
        Expr::new(span_1, 5),
        Some(Expr::new(span_2, 6)),
      ))),
    );
    assert_eq!(expr.span_of(&[ExprKey::String("collection".into())]).unwrap(), span_1);
    assert_eq!(expr.span_of(&[ExprKey::String("scope".into())]).unwrap(), span_2);

    let expr = Expr::new(
      Span::default(),
      ExprKind::Func(Box::new(QueryFunc::Add(VarArgs::new(vec![Expr::new(
        span_1,
        ExprKind::Int(0),
      )])))),
    );
    assert_eq!(expr.span_of(&[ExprKey::String("add".into()), ExprKey::Int(1)]).unwrap(), span_1);
  }
}
*/
