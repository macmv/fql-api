use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::ToTokens;
use syn::{
  parse::{Parse, ParseStream, Result},
  punctuated::Punctuated,
  token::Brace,
  Attribute, Expr, Ident, LitStr, Type,
};

struct QueryFuncInput {
  variants: Punctuated<QueryFuncVariant, Token![,]>,
}

struct QueryFuncVariant {
  attrs: Vec<Attribute>,
  name:  Ident,
  args:  Punctuated<QueryFuncArg, Token![,]>,
  block: Option<Expr>,
}

struct QueryFuncArg {
  name: LitStr,
  ty:   Type,
}

impl Parse for QueryFuncInput {
  fn parse(input: ParseStream) -> Result<Self> {
    Ok(QueryFuncInput { variants: input.parse_terminated(QueryFuncVariant::parse)? })
  }
}

impl Parse for QueryFuncVariant {
  fn parse(input: ParseStream) -> Result<Self> {
    let attrs = Attribute::parse_outer(input)?;
    let name: Ident = input.parse()?;
    let args;
    parenthesized!(args in input);
    Ok(QueryFuncVariant {
      attrs,
      name,
      args: args.parse_terminated(QueryFuncArg::parse)?,
      block: if input.peek(Brace) { Some(input.parse()?) } else { None },
    })
  }
}

impl Parse for QueryFuncArg {
  fn parse(input: ParseStream) -> Result<Self> {
    let name = input.parse()?;
    input.parse::<Token![:]>()?;
    let ty = input.parse()?;
    Ok(QueryFuncArg { name, ty })
  }
}

pub fn query_func(input: TokenStream) -> TokenStream {
  let input = parse_macro_input!(input as QueryFuncInput);

  let name = input.variants.iter().map(|v| &v.name).collect::<Vec<_>>();
  let name_str = input.variants.iter().map(|v| v.name.to_string()).collect::<Vec<_>>();
  let name_lower =
    input.variants.iter().map(|v| v.name.to_string().to_lowercase()).collect::<Vec<_>>();
  let attrs = input.variants.iter().map(|v| &v.attrs).collect::<Vec<_>>();
  let field_ty = input
    .variants
    .iter()
    .map(|v| v.args.iter().map(|a| &a.ty).collect::<Vec<_>>())
    .collect::<Vec<_>>();
  let arg = input
    .variants
    .iter()
    .map(|v| v.args.iter().map(|a| convert_arg(&a.name)).collect::<Vec<_>>())
    .collect::<Vec<_>>();
  let serialize_block = input.variants.iter().map(|v| {
    if let Some(b) = &v.block {
      b.to_token_stream()
    } else {
      let block = v.args.iter().map(|a| {
        let arg = convert_arg(&a.name);
        let arg_str = &a.name;
        if is_option(&a) {
          quote!(
            if let Some(v) = #arg {
              out.insert(#arg_str, to_value(v).unwrap());
            }
          )
        } else {
          quote!(out.insert(#arg_str, to_value(#arg).unwrap()))
        }
      });
      quote!(#(#block;)*)
    }
  });
  let parse_block = input.variants.iter().map(|v| {
    let name = &v.name;
    let expr = v
      .args
      .iter()
      .enumerate()
      .map(|(i, _)| {
        if is_variadic(&v.args[i]) {
          quote! {
            VarArgs::new(args.collect::<Result<Vec<_>, _>>()?)
          }
        } else if is_null(&v.args[i]) {
          quote! {
            ()
          }
        } else if is_option(&v.args[i]) {
          quote! {
            match args.next() {
              Some(v) => Some(v?),
              None => None,
            }
          }
        } else {
          let msg = format!("expected {} arguments, only got {} arguments", v.args.len(), i);
          quote! {
            args.next().unwrap_or_else(|| {
              Err(Error::new_from_span(ErrorVariant::CustomError { message: #msg.into() }, name.clone()))
            })?
          }
        }
      })
      .collect::<Vec<_>>();
    quote! {
      Ok(Self::#name(#(#expr),*))
    }
  });
  let span_of_block = input.variants.iter().map(|v| {
    let arg_str = v.args.iter().map(|a| &a.name);
    let arg_match = v.args.iter().map(|a| {
      let arg = convert_arg(&a.name);
      match &a.ty {
        // null type, appears when functions take no arguments
        syn::Type::Tuple(t) if t.elems.is_empty() => quote!(None),
        syn::Type::Path(p) if p.path.segments[0].ident == "Option" => {
          quote!(#arg.as_ref()?.span_of(&path[1..]))
        }
        syn::Type::Path(_) => quote!(#arg.span_of(&path[1..])),
        _ => unimplemented!("unexpected type"),
      }
    });
    quote!(match key {
      #(
        #arg_str => #arg_match,
      )*
      _ => None,
    })
  });

  let out = quote! {
    #[derive(Debug, Clone, PartialEq)]
    pub enum QueryFunc<'a> {
      #(
        #(#attrs)*
        #[doc = concat!(
          "See the [docs](https://docs.fauna.com/fauna/current/api/fql/functions/",
          #name_lower,
          ").",
        )]
        #name(#(#field_ty),*),
      )*
    }

    impl Serialize for QueryFunc<'_> {
      fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
      where
          S: Serializer,
      {
        let mut out = HashMap::new();
        match self {
          #(
            Self::#name(#(#arg),*) => {
              #serialize_block
            }
          )*
        }
        let mut map = serializer.serialize_map(Some(out.len()))?;
        for (k, v) in out {
          map.serialize_entry(k, &v)?;
        }
        map.end()
      }
    }

    impl<'a> QueryFunc<'a> {
      pub fn parse(name: Span<'a>, mut args: impl Iterator<Item = Result<Expr<'a>, Error<Rule>>>) -> Result<QueryFunc<'a>, Error<Rule>> {
        match name.as_str() {
          #(#name_str => {#parse_block})*
          _ => Err(Error::new_from_span(ErrorVariant::CustomError { message: "undefined function".into() }, name))
        }
      }
    }

    impl SpanOf for QueryFunc<'_> {
      fn span_of<'a>(&'a self, path: &[ExprKey]) -> Option<Span<'a>> {
        if path.is_empty() {
          return None;
        }
        let key = path[0].as_str()?;
        match self {
          #(
            Self::#name(#(#arg),*) => {
              #span_of_block
            }
          )*
        }
      }
    }
  };

  // Prints the output, formatted
  // let mut p =
  //   std::process::Command::new("rustfmt").stdin(std::process::Stdio::piped()).
  // spawn().unwrap(); std::io::Write::write_all(p.stdin.as_mut().unwrap(),
  // out.to_string().as_bytes()).unwrap(); p.wait_with_output().unwrap();

  out.into()
}

fn convert_arg(arg: &LitStr) -> Ident {
  Ident::new(
    match arg.value().as_str() {
      "do" => "d",
      "else" => "el",
      "if" => "i",
      "in" => "i",
      "let" => "l",
      "match" => "m",
      "ref" => "r",
      "self" => "slf",
      v => &v,
    },
    Span::call_site(),
  )
}

fn is_option(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Path(p) => p.path.segments.first().unwrap().ident == "Option",
    _ => false,
  }
}
fn is_null(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Tuple(t) => t.elems.is_empty(),
    _ => false,
  }
}
fn is_variadic(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Path(p) => p.path.segments.first().unwrap().ident == "VarArgs",
    _ => false,
  }
}
