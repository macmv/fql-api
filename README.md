# FQL API

This is a tool written in Rust in order to make FQL calls simpler.

### How to run

Install [rust](https://www.rust-lang.org/learn/get-started), and then clone
this repository:

```
git clone https://gitlab.com/macmv/fql-api.git
cd fql-api
```

To run the proxy in debug mode (connects to a local database), run this command:

```
cargo run
```

To run the proxy in release mode (connects to `db.fauna.com`), run this command:

```
cargo run --release
```

To run the internal tests (this is a fast, non-complete test setup):

```
cargo test
```

To run the api tester program (this test is slow, but tests the entire network,
including starting a local fauna instance to test against):

```
cargo run --bin fql-test
```

### Usage

There are examples for a bunch of languages in the
[examples](https://gitlab.com/macmv/fql-api/-/tree/main/examples) directory.
All of these examples load
[examples/queries/simple.fql](https://gitlab.com/macmv/fql-api/-/tree/main/examples/queries/simple.fql).
See below for how to test some queries from the command line.

I tested this using a tool called [`httpie`](https://httpie.io/cli). The
[examples/queries](https://gitlab.com/macmv/fql-api/-/tree/main/examples/misc)
directory contains a bunch of premade query files. Here is how to run one of the
examples:

```
http POST http://localhost:8000/execute\?number=5 \
  "Authorization: Bearer secret" \
  < examples/queries/misc/add.fql
```

This will run the `add.fql` query. This query uses an HTTP query parameter, which
is retrieved through the `URLQueryInt` function. If everything works as expected,
this should return `8`. See [here](https://gitlab.com/macmv/fql-api/-/blob/main/examples/queries/misc/add.fql)
to view the query example online.

The `Authorization` header in the above example is set to `Bearer secret`. This
is what is needed to test against a local fauna container. If you want to use the
real database, run the proxy in release mode, and replace `secret` will your actual
secret key. For example, if your key was `abcd`, you would set the `Authorization`
header to `Bearer abcd`.

Errors are parsed by the proxy and returned in a much more readable manner. You can
test a parse error with `examples/parse-error.fql`, and a runtime error with
`examples/runtime-error.fql`. This will print the output of a parse error:

```
http POST localhost:8000/execute \
  "Authorization: Bearer secret" \
  < examples/queries/misc/parse-error.fql
```

The error message here is meant to be printed directly, which you can do with
[`jq`](https://stedolan.github.io/jq/):

```
http POST localhost:8000/execute \
  "Authorization: Bearer secret" \
  < examples/queries/misc/parse-error.fql \
  | jq ".errors[0].description" -r
```

### Configuration

When you start the proxy, it will list out it's own configuration. This includes
the IP and port that it is hosting the server on. If you need to change any of
these settings, they can be found in `Rocket.toml`.

### Error format

Any errors returned by the API should look like this:

```json
{
  "errors": [{
    "code": "invalid argument",
    "description": "Number expected, String provided.",
    "printable_description": "<underlined source>",
    "span": {
      "start": {
        "line": 1,
        "col": 3
      },
      "end": {
        "line": 1,
        "col": 8
      }
    }
  }]
}
```

There can always be multiple errors returned, so make sure that is handled correctly.
Anytime there is a non-200 status code, this error format will be used. If the status
code is 200, then the returned value will still be valid json, but it will always be
the query result. So, if you always return an Integer from your query, you can simply
parse the response text as an integer. This will always work, assuming you get status
code 200 from the proxy.
